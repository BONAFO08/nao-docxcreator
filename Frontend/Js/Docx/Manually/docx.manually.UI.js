
let companyName = [];
let tableBasicInfoAux = [
    { name: 'product', type: 'text' },
    { name: 'quantity', type: 'number' },
    { name: 'CU yen', type: 'number' },
    { name: 'CU dollar', type: 'number' },
    { name: 'CT yen', type: 'number' },
    { name: 'CT dollar', type: 'number' },
    { name: 'cert', type: 'text' },
    { name: 'annot', type: 'text' },
];
let tableBasicInfoOriginal = [
    { name: 'product', type: 'text' },
    { name: 'quantity', type: 'number' },
    { name: 'CU yen', type: 'number' },
    { name: 'CU dollar', type: 'number' },
    { name: 'CT yen', type: 'number' },
    { name: 'CT dollar', type: 'number' },
    { name: 'cert', type: 'text' },
    { name: 'annot', type: 'text' },
];
let presentationText = '';
let presentationTextArr = [];
const tableInfo = {};


const sendData = () => {
    const firstValidator = validatingMinimuData();

    // if (firstValidator) {

    console.log(companyName);
    console.log(tableInfo);

    disableButtons();

    const divDataContainer = document.getElementById(`docxManuallyContainer`)
    const heightContainer = 70 + (20 * companyName.length);


    createFirstContainerSendData(divDataContainer, heightContainer);

    createSecondContainerSendData(divDataContainer, heightContainer);

    createThirdContainerSendData(divDataContainer, heightContainer);


    // }

}



const createDOCXmanuallyUI = () => {
    const docxManuallyContainer = html('div', {}, { id: 'docxManuallyContainer' });

    html('h3', {}, { id: 'titleDocxManually', textContent: 'DATA UPLOADER (MANUALLY)' }, docxManuallyContainer)
    html('br', {}, {}, docxManuallyContainer)


    const configurationContainer = html('div', {}, { id: 'configurationContainer' });

    html('h3', { fontSize: "5vh", margin: '5vh 0vh 0vh 0vh' }, { class: 'subtitleDocxManually', textContent: '------------------------------------------------------------------------------------------------------------------------------------' }, configurationContainer)
    html('h3', {}, { class: 'subtitleDocxManually', textContent: 'PLASE INTRODUCE YOUR CONFIGURATIONS' }, configurationContainer)
    html('br', {}, {}, configurationContainer)

    html('div', {}, { id: 'divDataContainerConfig' }, configurationContainer)

    html('br', {}, {}, configurationContainer)

    const buttonConfigurate = html('button', { margin: '3vh 0vh 0vh 0vh' }, { class: 'buttonDocxManually', textContent: `CONFIGURATE CONSTANTS` }, configurationContainer)

    buttonConfigurate.addEventListener('click', () => {
        createDOCXmanuallyConfigurationsPresentationTextContainer()
    })

    html('br', {}, {}, configurationContainer)


    const buttonPersonalData = html('button', { margin: '5vh 0vh 0vh 0vh' }, { class: 'buttonDocxManually', textContent: `SHOW PERSONAL DATA` }, configurationContainer)

    buttonPersonalData.addEventListener('click', () => {
        createDOCXmanuallyShowUserData()
    })

    const companyNameContainer = html('div', {}, { id: 'companyNameContainer' });

    html('h3', { fontSize: "5vh", margin: '5vh 0vh 0vh 0vh' }, { class: 'subtitleDocxManually', textContent: '------------------------------------------------------------------------------------------------------------------------------------' }, companyNameContainer)
    html('h3', {}, { class: 'subtitleDocxManually', textContent: 'PLASE INTRODUCE THE NAME/S OF THE COMPANY/IES' }, companyNameContainer)
    html('br', {}, {}, companyNameContainer)

    html('div', {}, { id: 'divDataContainerCompanyName' }, companyNameContainer)

    html('br', {}, {}, companyNameContainer)

    const buttonCompanyName = html('button', { margin: '3vh 0vh 0vh 0vh' }, { class: 'buttonDocxManually', textContent: `ADD COMPANY'S NAME` }, companyNameContainer)

    buttonCompanyName.addEventListener('click', () => {
        createDOCXmanuallyCompanyNameInputContainer()
        disableButtons()
    })


    html('br', {}, {}, companyNameContainer)

    const sendDataButton = html('button', { margin: '10vh 0vh 0vh 0vh' }, { class: 'buttonDocxManually', textContent: `SEND DATA` }, companyNameContainer)

    sendDataButton.addEventListener('click', () => {
        sendData();
    })

}


// const inputContainer = html('div', {}, {id: 'inputContainer'})



