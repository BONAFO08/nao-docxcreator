

const closeInputContainer = () => {
    try {
        document.getElementById('inputContainer').remove()
    } catch (error) {

    }
}


const disableButtons = () => {
    const buttons = Array.prototype.slice.call(document.getElementsByTagName('button'));
    buttons.map(button => {
        if (!button.className.includes('Inside')) {
            button.disabled = true
            button.style.opacity = 0.2;
            button.style.cursor = 'default'
        }

    })
}

const avaribleButtons = () => {


    const buttons = Array.prototype.slice.call(document.getElementsByTagName('button'));
    buttons.map(button => {
        button.disabled = false
        button.style.opacity = 1;
        button.style.cursor = 'pointer'
    })
}

const highlightErrorInputsDOCXmanually = () => {
    const inputs = Array.prototype.slice.call(document.getElementsByClassName('inputDocxManuallyInside'));
    inputs.map(input => input.style.border = '.5vh solid #c52525');
}


const addHoverButtonUpdate = (imageSetUrls) => {
    const buttons = Array.prototype.slice.call(document.getElementsByClassName('buttonUpdateDocxManuallyInside'));
    buttons.map(button => {
        button.style.backgroundImage = `url(${imageSetUrls.leave})`;

        button.onmouseenter = () => {
            button.style.backgroundColor = '#d87e18';
            button.style.backgroundImage = `url(${imageSetUrls.enter})`;
        }
        button.onmouseleave = () => {
            button.style.backgroundColor = '#000000';
            button.style.backgroundImage = `url(${imageSetUrls.leave})`;
        }


    })

}



const addConfirmHover = (confirmButton) => {
    confirmButton.addEventListener('mouseenter', () => {
        confirmButton.style.backgroundColor = '#1f6719';
        confirmButton.style.color = '#000000';
        confirmButton.style.border = '.5vh solid #40a737';
    })

    confirmButton.addEventListener('mouseleave', () => {
        confirmButton.style.backgroundColor = '#000000';
        confirmButton.style.color = '#d87e18';
        confirmButton.style.border = '.5vh solid #d87e18';
    })
}


const addCancelHover = (cancelButton) => {
    cancelButton.addEventListener('mouseenter', () => {
        cancelButton.style.backgroundColor = '#881e1e';
        cancelButton.style.color = '#000000';
        cancelButton.style.border = '.5vh solid #b72121';
    })

    cancelButton.addEventListener('mouseleave', () => {
        cancelButton.style.backgroundColor = '#000000';
        cancelButton.style.color = '#d87e18';
        cancelButton.style.border = '.5vh solid #d87e18';
    })
}


