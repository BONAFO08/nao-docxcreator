

//CREATE WINDOW FOR ASK TO USER A NEW COMPANY NAME
const createDOCXmanuallyCompanyNameInputContainer = () => {
    const docxManuallyContainer = document.getElementById('docxManuallyContainer');
    const inputContainer = html('div', { margin: `${5 + (companyName.length * 60)}vh 0vh 0vh 60vh` }, { id: 'inputContainer' }, docxManuallyContainer)

    html('h3', {}, { class: 'titleDocxManuallyInside', textContent: 'COMPANY NAME' }, inputContainer)
    html('br', {}, {}, inputContainer)

    const inputCompanyName = html('input', { margin: '5vh 0vh 0vh 0vh' }, { class: 'inputDocxManuallyInside', name: 'companyName', placeholder: 'Company Name' }, inputContainer)
    html('br', {}, {}, inputContainer)

    const responseText = html('h3', { margin: '2vh 0vh 0vh 0vh', opacity: 0 }, { class: 'responseTextDocxManuallyInside', textContent: 'NOTHING' }, inputContainer)
    html('br', {}, {}, inputContainer)


    const buttonSendCompanyName = html('button', { margin: '4vh 0vh 0vh 0vh' }, { class: 'buttonDocxManuallyInside', textContent: `SAVE` }, inputContainer)



    buttonSendCompanyName.addEventListener('click', () => {
        const response = validateCompanyName(inputCompanyName.value.trim())
        if (response.boolean) {
            companyName.push(inputCompanyName.value.trim())
            createTableEmptyEntry(inputCompanyName.value.trim());
            closeInputContainer()
            avaribleButtons()
            createDOCXmanuallyCompanyNameDataContainer()
        } else {
            responseText.textContent = response.text;
            responseText.style.opacity = 1;
            highlightErrorInputsDOCXmanually()
        }
    })

    const buttonCancelCompanyName = html('button', { margin: '0vh 0vh 0vh 10vh' }, { class: 'buttonDocxManuallyInside', textContent: `CANCEL` }, inputContainer)

    buttonCancelCompanyName.addEventListener('click', () => {
        closeInputContainer()
        avaribleButtons()
    })

}


//DELETE A COMPANY NAME AND ITS CARD
const deleteDOCXmanuallyCompanyNameNameCard = (name) => {
    const userConfirm = prompt(`Are you sure you want to remove ${name}? \n Please, write the "${name}"`)
    if (userConfirm == name) {
        companyName = companyName.filter(names => names != name)
        createDOCXmanuallyCompanyNameDataContainer()
    } else {
        alert('Elimination canceled')
    }
}

//CREATE WINDOW FOR ASK TO USER TO MODIFY A SPECIFIC COMPANY NAME
const modifyDOCXmanuallyCompanyNameInputContainer = (index, oldName) => {
    const docxManuallyContainer = document.getElementById('docxManuallyContainer');
    const inputContainer = html('div', { margin: '5vh 0vh 0vh 60vh' }, { id: 'inputContainer' }, docxManuallyContainer)

    html('h3', {}, { class: 'titleDocxManuallyInside', textContent: `MODIFY : ${companyName[index]}` }, inputContainer)
    html('br', {}, {}, inputContainer)

    const inputCompanyName = html('input', { margin: '5vh 0vh 0vh 0vh' }, { class: 'inputDocxManuallyInside', name: 'companyName', placeholder: 'New Company Name' }, inputContainer)
    html('br', {}, {}, inputContainer)

    const responseText = html('h3', { margin: '2vh 0vh 0vh 0vh', opacity: 0 }, { class: 'responseTextDocxManuallyInside', textContent: 'NOTHING' }, inputContainer)
    html('br', {}, {}, inputContainer)


    const buttonSendCompanyName = html('button', { margin: '4vh 0vh 0vh 0vh' }, { class: 'buttonDocxManuallyInside', textContent: `SAVE` }, inputContainer)

    buttonSendCompanyName.addEventListener('click', () => {
        const response = validateCompanyName(inputCompanyName.value.trim())
        if (response.boolean) {
            companyName[index] = inputCompanyName.value.trim()
            const oldCompanyNameInfo = tableInfo[oldName];
            tableInfo[companyName[index]] = oldCompanyNameInfo;
            delete tableInfo[oldName];
            closeInputContainer()
            avaribleButtons()
            createDOCXmanuallyCompanyNameDataContainer()
        } else {
            responseText.textContent = response.text;
            responseText.style.opacity = 1;
            highlightErrorInputsDOCXmanually()
        }
    })

    const buttonCancelCompanyName = html('button', { margin: '0vh 0vh 0vh 10vh' }, { class: 'buttonDocxManuallyInside', textContent: `CANCEL` }, inputContainer)

    buttonCancelCompanyName.addEventListener('click', () => {
        closeInputContainer()
        avaribleButtons()
    })
}

//SEARCHING THE INDEX OF THE NAME AND CALLING THE MODIFY COMPANY NAME'S WINDOW
const modifyDOCXmanuallyCompanyNameNameCard = (name) => {
    const indexOfName = companyName.indexOf(name);
    disableButtons()
    modifyDOCXmanuallyCompanyNameInputContainer(indexOfName, name)
}


//VALIDATE IF THE USER HAS ALREADY ASSIGNED TABLE DATA TO A COMPANY NAME
const dataTableExist = (name) => {
    try {
        const validatorData = tableInfo[name]
        if (validatorData != undefined) {
            return true
        } else {
            return false
        }
    } catch (error) {
        return false
    }
}



//VALIDATE IF THE USER HAS ALREADY ASSIGNED DEADLINE TO A COMPANY NAME
const dateExist = (name) => {
    try {
        const validatorData = tableInfo[name].date
        if (validatorData != undefined) {
            return true
        } else {
            return false
        }
    } catch (error) {
        return false
    }
}





//CREATING A COMPANY NAME CARD
const createDOCXmanuallyCompanyNameNameCard = (name) => {
    const divDataContainerCompanyName = document.getElementById('divDataContainerCompanyName')
    const container = html('div', {}, { id: `CompanyNameDivContainer${name}`, class: 'companyNameContainer' }, divDataContainerCompanyName)
    html('h3', { margin: '3vh 0vh 0vh 2vh' }, { class: 'textCompanyNames', id: `CompanyNameTextContainer${name}`, textContent: name }, container)


    html('br', {}, {}, container)
    const Modifybutton = html('button', { margin: '5vh 0vh 0vh 10vh' }, { class: 'buttonCompanyNames', id: `CompanyNameButtonModifyContainer${name}`, textContent: `MODIFY "${name}"` }, container)

    Modifybutton.addEventListener('click', () => {
        modifyDOCXmanuallyCompanyNameNameCard(name)
    })

    const Deletebutton = html('button', { margin: '0vh 0vh 0vh 10vh' }, { class: 'buttonCompanyNames', id: `CompanyNameButtonDeleteContainer${name}`, textContent: `DELETE "${name}"` }, container)

    Deletebutton.addEventListener('click', () => {
        deleteDOCXmanuallyCompanyNameNameCard(name)
    })

    html('br', {}, {}, container)



    const ShowDataTablebutton = html('button', { margin: '5vh 0vh 0vh 10vh' }, { class: 'buttonCompanyNames', id: `CompanyNameButtonShowDataTableContainer${name}`, textContent: `SHOW DATA TABLE "${name}"` }, container)

    ShowDataTablebutton.addEventListener('click',()=>{
        showDataTableDOCXmanually(name);
    })


    const AddDateButton = html('button', { margin: '5vh 0vh 0vh 10vh' }, { class: 'buttonCompanyNames', id: `CompanyNameButtonAddDataContainer${name}`, textContent: `${(dateExist(name)) ? ('MODIFY DEADLINE') : ('ADD DEADLINE')} "${name}"` }, container)

    AddDateButton.addEventListener('click', () => {
        showDOCXmanuallyDeadLineMenu(name);
    })
}

//CREATING ALL THE COMPANY NAMES' CARDS
const createDOCXmanuallyCompanyNameDataContainer = () => {
    const divDataContainerCompanyName = document.getElementById('divDataContainerCompanyName')
    clearDiv(divDataContainerCompanyName)
    companyName.map(name => {
        createDOCXmanuallyCompanyNameNameCard(name)
    })
}


