const extraValidationsUserData = (data, type) => {
    switch (type) {
        case 'email': return (data.includes('@') && data.includes('.')) ? (data) : (false);

        case 'dni': return (isNaN(data)) ? (false) : (data);
        default:
            break;
    }
}

const validateUserData = async (newData, userData) => {
    const msj = { boolean: false, text: '' };
    let changes = { boolean: false, newData: {} };
    const userDataKey = Object.keys(userData);
    userDataKey.map(key => {
        const cleanData = (newData[key] != '' && newData[key] != undefined) ? (newData[key].trim()) : ('')
        if (cleanData != '') {
            if (cleanData != userData[key]) {
                const lastValidations = extraValidationsUserData(cleanData, key);
                if (lastValidations != false) {
                    changes.newData[key] = cleanData
                    changes.boolean = changes.boolean || true;
                } else {
                    changes.boolean = changes.boolean || false;
                }
            } else {
                changes.boolean = changes.boolean || false;
            }
        } else {
            changes.boolean = changes.boolean || false;
        }
    })

    if (changes.boolean == true) {
        const response = await saveNewUserData(changes.newData);
        msj.text = response.msj
        msj.status = response.status
        return msj
    }

    msj.text = 'Nothing to change'
    return msj
}

