const getUserData = async () => {
    let resp;
    let myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    myHeaders.append("Authorization", window.sessionStorage.getItem('token'));

    let requestOptions = {
        method: 'GET',
        headers: myHeaders,
        redirect: 'follow'
    };

    await fetch(`${backendURL}/user/userData`, requestOptions)
        .then(response => response.json())
        .then(result => resp = result)
        .catch(error => console.log('error', error));
    return resp;

}

const saveNewUserData = async (newData) => {
    let resp;
    let myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    myHeaders.append("Authorization", window.sessionStorage.getItem('token'));

    let requestOptions = {
        method: 'PUT',
        headers: myHeaders,
        redirect: 'follow',
        body: JSON.stringify(newData),
    };

    await fetch(`${backendURL}/user/updateUser`, requestOptions)
        .then(response => response.json())
        .then(result => resp = result)
        .catch(error => console.log('error', error));
    return resp;

}