

const createDOCXmanuallyEquivalenceTable = () => {
    const docxManuallyContainer = document.getElementById('docxManuallyContainer');
    const inputContainer = html('div', {
        margin: '0vh 0vh 0vh 60vh',
        width: '100vh',
        height: '90vh',
        zIndex: 2000,
    },

        { id: 'inputContainer' }, docxManuallyContainer)

    html('br', {}, {}, inputContainer)
    html('h3', { fontSize: "3vh" }, { class: 'titleDocxManuallyInside', textContent: `TABLE OF EQUIVALENCES FOR THE PRESENTATION TEXT, ALWAYS REMEMBER TO PUT { BETWEEN THE TEXT YOU WANT TO REPLACE}` }, inputContainer)
    html('br', {}, {}, inputContainer)
    html('br', {}, {}, inputContainer)

    html('h3', { fontSize: "3vh" }, { class: 'titleDocxManuallyInside', textContent: `EXAMPLE: Hello {company}, you're going to receive an email from me today {date}!` }, inputContainer)
    html('br', {}, {}, inputContainer)
    html('br', {}, {}, inputContainer)
    html('br', {}, {}, inputContainer)
    
    html('h3', { fontSize: "4vh" }, { class: 'titleDocxManuallyInside', textContent: `{n} or {N} == Breakline` }, inputContainer)
    html('br', {}, {}, inputContainer)
    html('br', {}, {}, inputContainer)

    html('h3', { fontSize: "4vh" }, { class: 'titleDocxManuallyInside', textContent: `{company} == COMPANY NAME` }, inputContainer)
    html('br', {}, {}, inputContainer)
    html('br', {}, {}, inputContainer)

    html('h3', { fontSize: "4vh" }, { class: 'titleDocxManuallyInside', textContent: `{date} == DEADLINE` }, inputContainer)
    html('br', {}, {}, inputContainer)

    const buttonEquivalenceTable = html('button', { margin: '4vh 0vh 0vh 0vh' }, { class: 'buttonDocxManuallyInside', textContent: `OK!` }, inputContainer)
    buttonEquivalenceTable.addEventListener('click', () => {
        inputContainer.remove();
    })
}

//CREATE WINDOW TO ASK PRESENTATION TEXT
const createDOCXmanuallyConfigurationsPresentationTextContainer = () => {

    disableButtons()


    const docxManuallyContainer = document.getElementById('docxManuallyContainer');
    const inputContainer = html('div', {
        margin: '0vh 0vh 0vh 60vh',
        width: '100vh',
        height: '90vh',
    },
        { id: 'inputContainer' }, docxManuallyContainer)

    html('h3', {}, { class: 'titleDocxManuallyInside', textContent: `CONFIGURATIONS ` }, inputContainer)
    html('br', {}, {}, inputContainer)


    html('h3', { margin: '5vh 0vh 0vh 0vh' }, { class: 'titleDocxManuallyInside', textContent: `PRESENTATION TEXT` }, inputContainer)
    html('br', {}, {}, inputContainer)

    const textareaPresentationText = html('textarea',
        {
            margin: '5vh 0vh 0vh 0vh',
            height: '25vh',
            fontSize: '3vh',
            whiteSpace: 'pre',
        },
        {
            class: 'inputDocxManuallyInside', name: 'companyName', placeholder: 'Presentation Text'
        }, inputContainer)


    html('br', {}, {}, inputContainer)

    const responseText = html('h3', { margin: '2vh 0vh 0vh 0vh', opacity: 0 }, { class: 'responseTextDocxManuallyInside', textContent: 'NOTHING' }, inputContainer)
    html('br', {}, {}, inputContainer)



    const buttonEquivalenceTable = html('button', { margin: '4vh 0vh 0vh 0vh' }, { class: 'buttonDocxManuallyInside', textContent: `TABLE OF EQUIVALENCES` }, inputContainer)

    buttonEquivalenceTable.addEventListener('click', () => {
        createDOCXmanuallyEquivalenceTable();
    })

    html('br', {}, {}, inputContainer)
    const buttonChangeToTablePage = html('button', { margin: '4vh 0vh 0vh 0vh' }, { class: 'buttonDocxManuallyInside', textContent: `TABLE BASIC INFO` }, inputContainer)

    buttonChangeToTablePage.addEventListener('click', () => {
        inputContainer.remove()
        createDOCXmanuallyConfigurationsBasicTableContainer()
    })



    html('br', {}, {}, inputContainer)

    const buttonSendCompanyName = html('button', { margin: '4vh 0vh 0vh 0vh' }, { class: 'buttonDocxManuallyInside', textContent: `SAVE` }, inputContainer)

    buttonSendCompanyName.addEventListener('click', () => {
        presentationText = textareaPresentationText.value;

        tableBasicInfoOriginal = [];
        tableBasicInfoAux.map(tableInfo => tableBasicInfoOriginal.push(tableInfo))

        alert('saved!');
        inputContainer.remove()
        avaribleButtons()
    })

    const buttonCancelCompanyName = html('button', { margin: '0vh 0vh 0vh 10vh' }, { class: 'buttonDocxManuallyInside', textContent: `CANCEL` }, inputContainer)

    buttonCancelCompanyName.addEventListener('click', () => {
        tableBasicInfoAux = [];
        tableBasicInfoOriginal.map(tableInfo => tableBasicInfoAux.push(tableInfo))

        inputContainer.remove()
        avaribleButtons()
    })

    if (presentationText != '') {
        textareaPresentationText.value = presentationText;
    }

}

//CREATE WINDOW TO ASK BASIC TABLE INFORMATION
const createDOCXmanuallyConfigurationsBasicTableContainer = () => {
    const docxManuallyContainer = document.getElementById('docxManuallyContainer');

    const inputContainer = html('div', {
        margin: '0vh 0vh 0vh 60vh',
        width: '120vh',
        height: `${(20 + (40 * tableBasicInfoAux.length))}vh`,
    },
        { id: 'inputContainer' }, docxManuallyContainer)

    html('h3', {}, { class: 'titleDocxManuallyInside', textContent: `CONFIGURATIONS ` }, inputContainer)
    html('br', {}, {}, inputContainer)

    tableBasicInfoAux.map(talbeInfo => {
        const basicTableContainer = html('div', {
        },
            { id: 'basicTableContainer', class: 'tableBasicInfoContainer' }, inputContainer)
        html('h3', { margin: '3vh 4vh 0vh 0vh', textAlign: 'center', fontSize: '3.5vh' }, { class: 'textDocxManuallyInside', textContent: `>>>${talbeInfo.name}<<<` }, basicTableContainer)
        html('br', {}, {}, basicTableContainer)
        html('h3', { margin: '3vh 4vh 0vh 0vh', textAlign: 'center', fontSize: '3.5vh' }, { class: 'textDocxManuallyInside', textContent: `type : ${talbeInfo.type}` }, basicTableContainer)
        html('br', {}, {}, basicTableContainer)


        const buttonDeleteBasicTable = html('button', { margin: '4vh 0vh 0vh 0vh' }, { class: 'buttonDocxManuallyInside', textContent: `X` }, basicTableContainer)
        buttonDeleteBasicTable.addEventListener('click', () => {
            tableBasicInfoAux = tableBasicInfoAux.filter(basicInfo => basicInfo.name != talbeInfo.name);
            inputContainer.remove()
            createDOCXmanuallyConfigurationsBasicTableContainer()
        })

        html('br', {}, {}, inputContainer)
    })

    const newPropietyContainer = html('div', {
    },
        {}, inputContainer)

    const buttonAddBasicTable = html('button', { margin: '4vh 0vh 0vh 0vh' }, { class: 'buttonDocxManuallyInside', textContent: `+` }, newPropietyContainer)

    html('br', {}, {}, inputContainer)

    buttonAddBasicTable.addEventListener('click', () => {
        const basicTableContainer = html('div', {
            padding: "0vh 3vh 5vh 3vh"
        },
            { id: 'basicTableContainer', class: 'tableBasicInfoContainer' }, newPropietyContainer)

        const inputNewPropietyName = html('input', { margin: '3vh 4vh 0vh 0vh', width: '50vh', textAlign: 'center' }, { class: 'inputDocxManuallyInside', placeholder: 'New Propiet Name' }, basicTableContainer)
        html('br', {}, {}, basicTableContainer)

        html('input', { margin: '3vh 4vh 0vh 0vh', width: "4vh", height: "2.8vh", textAlign: 'center', cursor: 'pointer' }, { class: 'inputDocxManuallyInside', placeholder: 'New Propiet Name', type: 'radio', name: 'type', id: 'newPropietyType', value: 'text' }, basicTableContainer)
        html('h3', { margin: '3vh 4vh 0vh 0vh', textAlign: 'center', fontSize: '3.5vh' }, { class: 'textDocxManuallyInside', textContent: `TEXT` }, basicTableContainer)

        html('input', { margin: '3vh 4vh 0vh 0vh', width: "4vh", height: "2.8vh", textAlign: 'center', cursor: 'pointer' }, { class: 'inputDocxManuallyInside', placeholder: 'New Propiet Name', type: 'radio', name: 'type', id: 'newPropietyType', value: 'number' }, basicTableContainer)
        html('h3', { margin: '3vh 4vh 0vh 0vh', textAlign: 'center', fontSize: '3.5vh' }, { class: 'textDocxManuallyInside', textContent: `NUMBER` }, basicTableContainer)

        html('input', { margin: '3vh 4vh 0vh 0vh', width: "4vh", height: "2.8vh", textAlign: 'center', cursor: 'pointer' }, { class: 'inputDocxManuallyInside', placeholder: 'New Propiet Name', type: 'radio', name: 'type', id: 'newPropietyType', value: 'email' }, basicTableContainer)
        html('h3', { margin: '3vh 4vh 0vh 0vh', textAlign: 'center', fontSize: '3.5vh' }, { class: 'textDocxManuallyInside', textContent: `EMAIL` }, basicTableContainer)

        html('br', {}, {}, basicTableContainer)

        const buttonSaveNewPropiety = html('button', { margin: '4vh 0vh 0vh 0vh' }, { class: 'buttonDocxManuallyInside', textContent: `SAVE` }, basicTableContainer)

        buttonSaveNewPropiety.addEventListener('click', () => {
            const newPropietyName = (inputNewPropietyName.value).trim();
            const newPropietyType = document.querySelector('input[name=type]:checked').value;
            if (newPropietyName != '') {
                tableBasicInfoAux.push({ name: newPropietyName, type: newPropietyType });
                inputContainer.remove()
                createDOCXmanuallyConfigurationsBasicTableContainer()
            }

        })

        const buttonDeleteNewPropiety = html('button', { margin: '4vh 0vh 0vh 5vh' }, { class: 'buttonDocxManuallyInside', textContent: `CANCEL` }, basicTableContainer)

        buttonDeleteNewPropiety.addEventListener('click', () => {
            basicTableContainer.remove();
            const buttonAddBasicTable = html('button', { margin: '4vh 0vh 0vh 0vh' }, { class: 'buttonDocxManuallyInside', textContent: "+" }, newPropietyContainer)
            buttonAddBasicTable.style.display = 'inline-block';
            inputContainer.remove()
            createDOCXmanuallyConfigurationsBasicTableContainer()
        })

        buttonAddBasicTable.remove();
    })

    const buttonChangeToTablePage = html('button', { margin: '4vh 0vh 0vh 0vh' }, { class: 'buttonDocxManuallyInside', textContent: `TABLE BASIC INFO` }, inputContainer)

    buttonChangeToTablePage.addEventListener('click', () => {
        inputContainer.remove()
        createDOCXmanuallyConfigurationsPresentationTextContainer()
    })

    html('br', {}, {}, inputContainer)


    const buttonSendCompanyName = html('button', { margin: '4vh 0vh 0vh 0vh' }, { class: 'buttonDocxManuallyInside', textContent: `SAVE` }, inputContainer)

    buttonSendCompanyName.addEventListener('click', () => {
        tableBasicInfoOriginal = [];
        tableBasicInfoAux.map(tableInfo => tableBasicInfoOriginal.push(tableInfo))
        alert('saved!');
        inputContainer.remove()
        avaribleButtons()
    })

    const buttonCancelCompanyName = html('button', { margin: '0vh 0vh 0vh 10vh' }, { class: 'buttonDocxManuallyInside', textContent: `CANCEL` }, inputContainer)

    buttonCancelCompanyName.addEventListener('click', () => {
        tableBasicInfoAux = [];
        tableBasicInfoOriginal.map(tableInfo => tableBasicInfoAux.push(tableInfo))

        inputContainer.remove()
        avaribleButtons()
    })
}




//CREATE WINDOW TO SHOW USER DATA (AND MOD BASIC DATA)
const createDOCXmanuallyShowUserData = async () => {

    disableButtons()
    const userData = await getUserData()

    const imageSetUrlsCancelButton = {
        enter: '../Images/cancelOver.png',
        leave: '../Images/cancelOff.png'
    }

    const imageSetUrls = {
        enter: '../Images/modOver.png',
        leave: '../Images/modOff.png'
    }

    if (userData.status == 200) {
        const userDataKey = Object.keys(userData.msj);

        const docxManuallyContainer = document.getElementById('docxManuallyContainer');
        const inputContainer = html('div', {
            margin: '0vh 0vh 0vh 60vh',
            width: '100vh',
            height: '90vh',
        }, { id: 'inputContainer' }, docxManuallyContainer)


        html('h3', { margin: '1vh 0vh 0vh 0vh' }, { class: 'titleDocxManuallyInside', textContent: `${(userData.msj.username).toUpperCase()} DATA ` }, inputContainer)
        html('br', {}, {}, inputContainer)




        userDataKey.map(key => {



            if (key != 'username') {
                html('h3', { margin: '3vh 0vh 0vh 0vh' },
                    {
                        class: 'textDocxManuallyInside',
                        textContent: `${key.toUpperCase()} : ${(userData.msj[key])}`,
                        id: `${key}Text`
                    }, inputContainer);
                html('input', { margin: '3vh 0vh 0vh 0vh', display: 'none' },
                    {
                        class: 'inputDocxManuallyInside',
                        placeholder: `${(userData.msj[key])}`,
                        id: `${key}Input`,
                        type: (key == 'dni') ? ('number') : ('text')
                    }, inputContainer);

                let showing = 'text'

                html('button', {
                    margin: '4vh 0vh 0vh 5vh',
                }, { class: 'buttonUpdateDocxManuallyInside' }, inputContainer).addEventListener('click', (e) => {
                    const button = e.target
                    if (showing == 'text') {
                        document.getElementById(`${key}Text`).style.display = 'none';
                        document.getElementById(`${key}Input`).style.display = 'inline-block';

                        button.style.backgroundImage = `url(${imageSetUrlsCancelButton.leave})`;
                        button.onmouseenter = () => {
                            button.style.backgroundColor = '#d87e18';
                            button.style.backgroundImage = `url(${imageSetUrlsCancelButton.enter})`;
                        }
                        button.onmouseleave = () => {
                            button.style.backgroundColor = '#000000';
                            button.style.backgroundImage = `url(${imageSetUrlsCancelButton.leave})`;
                        }
                        showing = 'input'

                    } else {

                        document.getElementById(`${key}Input`).value = ''
                        document.getElementById(`${key}Text`).style.display = 'inline-block';
                        document.getElementById(`${key}Input`).style.display = 'none';

                        button.style.backgroundImage = `url(${imageSetUrls.leave})`;
                        button.onmouseenter = () => {
                            button.style.backgroundColor = '#d87e18';
                            button.style.backgroundImage = `url(${imageSetUrls.enter})`;
                        }
                        button.onmouseleave = () => {
                            button.style.backgroundColor = '#000000';
                            button.style.backgroundImage = `url(${imageSetUrls.leave})`;
                        }
                        showing = 'text'

                    }
                })

                html('br', {}, {}, inputContainer)

            }
        })

        addHoverButtonUpdate(imageSetUrls)

        const responseText = html('h3', { margin: '5vh 0vh 0vh 0vh', opacity: 0 }, { class: 'responseTextDocxManuallyInside', textContent: 'NOTHING' }, inputContainer)
        html('br', {}, {}, inputContainer)

        const buttonSendUserDataConfiguration = html('button', { margin: '6vh 0vh 0vh 0vh' }, { class: 'buttonDocxManuallyInside', textContent: `SAVE` }, inputContainer)

        buttonSendUserDataConfiguration.addEventListener('click', async () => {
            const inputs = Array.prototype.slice.call(document.getElementsByClassName('inputDocxManuallyInside'));
            let newData = {};
            inputs.map(input => {
                newData = { ...newData, [input.id.replace('Input', '')]: input.value }
            })
            const response = await validateUserData(newData, userData.msj)
            if (response.status == 200) {
                alert(response.text)
                inputContainer.remove()
                avaribleButtons()
            } else {
                responseText.style.opacity = 1
                responseText.textContent = response.text;
            }
        })

        const buttonCancelUserDataConfiguration = html('button', { margin: '4vh 0vh 0vh 10vh' }, { class: 'buttonDocxManuallyInside', textContent: `CANCEL` }, inputContainer)

        buttonCancelUserDataConfiguration.addEventListener('click', () => {
            inputContainer.remove()
            avaribleButtons()
        })

        html('br', {}, {}, inputContainer)
        const buttonClearDataUserDataConfiguration = html('button', { margin: '4vh 0vh 0vh 0vh' }, { class: 'buttonDocxManuallyInside', textContent: `CLEAR DATA` }, inputContainer)

        buttonClearDataUserDataConfiguration.addEventListener('click', () => {
            const inputs = Array.prototype.slice.call(document.getElementsByClassName('inputDocxManuallyInside'));
            inputs.map(input => input.value = '')
        })
    } else {
        alert('ERROR')
    }

}

