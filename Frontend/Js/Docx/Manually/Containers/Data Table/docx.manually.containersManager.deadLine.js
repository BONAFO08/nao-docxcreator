const showDOCXmanuallyDeadLineMenu = (companyName) => {
    console.log(tableInfo[companyName].date);
    if (tableInfo[companyName].dateFormat == undefined) {
        createDOCXmanuallyDeadLineMenu(companyName);
    } else {
        switch (tableInfo[companyName].dateFormat) {
            case 'japanese':
                createDOCXmanuallyDeadLineJapaneseFormatUI(companyName);
                break;

            case 'english':
                createDOCXmanuallyDeadLineEnglishFormatUI(companyName);
                break;

            case 'spanish':
                createDOCXmanuallyDeadLineSpanishFormatUI(companyName);
                break;
        }
    }
}

const createFormatUI = (companyName, format) => {
    closeInputContainer()
    disableButtons();
    const docxManuallyContainer = document.getElementById('docxManuallyContainer');
    const inputContainer = html('div', {
        margin: '40vh 0vh 0vh 40vh',
        width: '100vh',
        height: '50vh',
        zIndex: 2000,
    },
        { id: 'inputContainer' }, docxManuallyContainer)

    html('h3', {
        margin: '3vh 0vh 5vh 0vh',
    }, {
        class: 'textDocxManuallyInside',
        textContent: `${companyName} DEADLINE [${format.toUpperCase()}]`
    }, inputContainer);
    html('br', {}, {}, inputContainer)

    return inputContainer;

}


const createDOCXmanuallyDeadLineMenu = (companyName) => {
    disableButtons();
    const docxManuallyContainer = document.getElementById('docxManuallyContainer');
    const inputContainer = html('div', {
        margin: '40vh 0vh 0vh 40vh',
        width: '100vh',
        height: '90vh',
        zIndex: 2000,
    },
        { id: 'inputContainer' }, docxManuallyContainer)

    html('h3', {
        margin: '3vh 0vh 5vh 0vh',
    }, {
        class: 'textDocxManuallyInside',
        textContent: `${companyName} DEADLINE`
    }, inputContainer);
    html('br', {}, {}, inputContainer)

    html('h3', {
        margin: '3vh 0vh 5vh 0vh',
    }, {
        class: 'textDocxManuallyInside',
        textContent: 'Please select the date format'
    }, inputContainer);


    const JapaneseFormatSelectButton = html('button', {
        margin: '3vh 0vh 10vh 0vh',
    }, {
        class: 'buttonDocxManuallyInside',
        textContent: '日本 / Japanese / Japones'
    }, inputContainer);
    html('br', {}, {}, inputContainer)

    JapaneseFormatSelectButton.addEventListener('click', () => {
        createDOCXmanuallyDeadLineJapaneseFormatUI(companyName);
    })


    const EnglishFormatSelectButton = html('button', {
        margin: '0vh 0vh 10vh 0vh',
    }, {
        class: 'buttonDocxManuallyInside',
        textContent: '英 / English / Ingles'
    }, inputContainer);
    html('br', {}, {}, inputContainer)

    EnglishFormatSelectButton.addEventListener('click', () => {
        createDOCXmanuallyDeadLineEnglishFormatUI(companyName);
    })


    const SpanishFormatSelectButton = html('button', {
        margin: '0vh 0vh 10vh 0vh',
    }, {
        class: 'buttonDocxManuallyInside',
        textContent: '西語 / Spanish / Español'
    }, inputContainer);
    html('br', {}, {}, inputContainer)

    SpanishFormatSelectButton.addEventListener('click', () => {
        createDOCXmanuallyDeadLineSpanishFormatUI(companyName);
    })


    const cancelButton = html('button', {
        margin: '0vh 0vh 0vh 0vh',
    }, {
        class: 'buttonDocxManuallyInside',
        textContent: 'CANCEL'
    }, inputContainer);

    cancelButton.addEventListener('click', () => {
        closeInputContainer()
        avaribleButtons()
    })
}

const fromDateToArr = (date) => {
    let breaker = 10;
    const dateArr = [];
    let indexEND;
    let objectAux;

    for (let i = 0; i < breaker; i++) {
        indexEND = date.indexOf('/');
        objectAux = date.substring(0, indexEND + 1);
        date = date.replace(objectAux, '');
        objectAux = objectAux.replace('/', '');
        dateArr.push(objectAux);
        if (date != undefined && date.indexOf('/') == -1) {
            dateArr.push(date);
            i = 11;
        }
    }

    return dateArr;
}


const dateAddZeros = (number) => {
    return ((number.toString().length) == 1) ? ('0' + (number.toString())) : (number.toString());
}

const createDOCXmanuallyDeadLineJapaneseFormatUI = (companyName) => {
    const inputContainer = createFormatUI(companyName, 'Japanese');
    tableInfo[companyName].dateFormat = 'japanese';

    let dateArr = [];

    if (tableInfo[companyName].date != undefined) {
        dateArr = fromDateToArr(tableInfo[companyName].date);
    }

    const inputYear = html('input', {
        margin: '3vh 0vh 5vh 3vh',
        width: '15vh',
    }, {
        class: 'inputDocxManuallyInside',
        placeholder: `YYYY`,
        value: (dateArr[0] != undefined) ? (dateArr[0]) : (``),
        type: `number`,
        min: 1900,
        max: 2500,
    }, inputContainer);

    inputYear.addEventListener('change', () => {
        const inputValue = parseInt(inputYear.value);
        if (inputValue < 1900) inputYear.value = 1900;
        if (inputValue > 2500) inputYear.value = 2500;
    })

    html('h3', {
        margin: '0vh 2vh 0vh 2vh',
        color: '#187953',
        fontSize: '6vh',
    }, {
        class: 'textDocxManuallyInside',
        textContent: `年`
    }, inputContainer);

    const inputMonth = html('input', {
        margin: '0vh 0vh 0vh 3vh',
        width: '15vh',
    }, {
        class: 'inputDocxManuallyInside',
        placeholder: `MM`,
        value: (dateArr[1] != undefined) ? (dateArr[1]) : (``),
        type: `number`,
        min: 1,
        max: 12,
    }, inputContainer);

    inputMonth.addEventListener('change', () => {
        const inputValue = parseInt(inputMonth.value);
        if (inputValue < 1) inputMonth.value = 1;
        if (inputValue > 12) inputMonth.value = 12;
    })

    html('h3', {
        margin: '0vh 2vh 0vh 2vh',
        color: '#187953',
        fontSize: '6vh',
    }, {
        class: 'textDocxManuallyInside',
        textContent: `月`
    }, inputContainer);



    const inputDay = html('input', {
        margin: '0vh 0vh 0vh 3vh',
        width: '15vh',
    }, {
        class: 'inputDocxManuallyInside',
        placeholder: `DD`,
        value: (dateArr[2] != undefined) ? (dateArr[2]) : (``),
        type: `number`,
        min: 1,
        max: 31,
    }, inputContainer);


    inputDay.addEventListener('change', () => {
        const inputValue = parseInt(inputDay.value);
        if (inputValue < 1) inputDay.value = 1;
        if (inputValue > 31) inputDay.value = 31;
    })

    html('h3', {
        margin: '0vh 2vh 0vh 2vh',
        color: '#187953',
        fontSize: '6vh',
    }, {
        class: 'textDocxManuallyInside',
        textContent: `日`
    }, inputContainer);


    html('br', {}, {}, inputContainer)

    const backButton = html('button', {
        margin: '5vh 5vh 0vh 0vh',
    }, {
        class: 'buttonDocxManuallyInside',
        textContent: 'CHANGE FORMAT'
    }, inputContainer);

    backButton.addEventListener('click', () => {
        tableInfo[companyName].dateFormat = undefined;
        tableInfo[companyName].date = undefined;

        closeInputContainer()
        avaribleButtons()
        showDOCXmanuallyDeadLineMenu(companyName);
    })

    const confirmButton = html('button', {
        margin: '0vh 5vh 0vh 0vh',
    }, {
        class: 'buttonDocxManuallyInside',
        textContent: 'CONFIRM'
    }, inputContainer);

    confirmButton.addEventListener('click', () => {

        let validator = true;
        let inputs = [
            inputYear,
            inputMonth,
            inputDay,
        ];

        inputs.map(input => {
            if (input.value == '') {
                input.style.border = '.5vh solid #c52525';
                validator = validator && false;
            } else {
                input.style.border = '.5vh solid #d87e18';
            }
        })


        if (validator) {
            tableInfo[companyName].date = `${dateAddZeros(Math.abs(inputYear.value))}/${dateAddZeros(Math.abs(inputMonth.value))}/${dateAddZeros(Math.abs(inputDay.value))}`;
            closeInputContainer()
            avaribleButtons()
        } else {
            alert('Error.Missing Data')
        }

    })

    const cancelButton = html('button', {
        margin: '0vh 5vh 0vh 0vh',
    }, {
        class: 'buttonDocxManuallyInside',
        textContent: 'CANCEL'
    }, inputContainer);

    cancelButton.addEventListener('click', () => {
        if (tableInfo[companyName].date == undefined) {
            tableInfo[companyName].dateFormat = undefined;
        }
        closeInputContainer()
        avaribleButtons()
    })
}

const createDOCXmanuallyDeadLineEnglishFormatUI = (companyName) => {
    const inputContainer = createFormatUI(companyName, 'English');
    tableInfo[companyName].dateFormat = 'english';

    let dateArr = [];

    if (tableInfo[companyName].date != undefined) {
        dateArr = fromDateToArr(tableInfo[companyName].date);
    }


    const inputMonth = html('input', {
        margin: '0vh 0vh 0vh 3vh',
        width: '15vh',
    }, {
        class: 'inputDocxManuallyInside',
        placeholder: `MM`,
        value: (dateArr[1] != undefined) ? (dateArr[0]) : (``),
        type: `number`,
        min: 1,
        max: 12,
    }, inputContainer);

    inputMonth.addEventListener('change', () => {
        const inputValue = parseInt(inputMonth.value);
        if (inputValue < 1) inputMonth.value = 1;
        if (inputValue > 12) inputMonth.value = 12;
    })

    html('h3', {
        margin: '0vh 2vh 0vh 2vh',
        color: '#187953',
        fontSize: '4vh',
    }, {
        class: 'textDocxManuallyInside',
        textContent: `Month`
    }, inputContainer);


    const inputDay = html('input', {
        margin: '0vh 0vh 0vh 3vh',
        width: '15vh',
    }, {
        class: 'inputDocxManuallyInside',
        placeholder: `DD`,
        value: (dateArr[2] != undefined) ? (dateArr[1]) : (``),
        type: `number`,
        min: 1,
        max: 31,
    }, inputContainer);


    inputDay.addEventListener('change', () => {
        const inputValue = parseInt(inputDay.value);
        if (inputValue < 1) inputDay.value = 1;
        if (inputValue > 31) inputDay.value = 31;
    })

    html('h3', {
        margin: '0vh 2vh 0vh 2vh',
        color: '#187953',
        fontSize: '4vh',
    }, {
        class: 'textDocxManuallyInside',
        textContent: `Day`
    }, inputContainer);


    const inputYear = html('input', {
        margin: '3vh 0vh 5vh 3vh',
        width: '15vh',
    }, {
        class: 'inputDocxManuallyInside',
        placeholder: `YYYY`,
        value: (dateArr[0] != undefined) ? (dateArr[2]) : (``),
        type: `number`,
        min: 1900,
        max: 2500,
    }, inputContainer);

    inputYear.addEventListener('change', () => {
        const inputValue = parseInt(inputYear.value);
        if (inputValue < 1900) inputYear.value = 1900;
        if (inputValue > 2500) inputYear.value = 2500;
    })

    html('h3', {
        margin: '0vh 2vh 0vh 2vh',
        color: '#187953',
        fontSize: '4vh',
    }, {
        class: 'textDocxManuallyInside',
        textContent: `Year`
    }, inputContainer);


    html('br', {}, {}, inputContainer)

    const backButton = html('button', {
        margin: '5vh 5vh 0vh 0vh',
    }, {
        class: 'buttonDocxManuallyInside',
        textContent: 'CHANGE FORMAT'
    }, inputContainer);

    backButton.addEventListener('click', () => {
        tableInfo[companyName].dateFormat = undefined;
        tableInfo[companyName].date = undefined;

        closeInputContainer()
        avaribleButtons()
        showDOCXmanuallyDeadLineMenu(companyName);
    })

    const confirmButton = html('button', {
        margin: '0vh 5vh 0vh 0vh',
    }, {
        class: 'buttonDocxManuallyInside',
        textContent: 'CONFIRM'
    }, inputContainer);

    confirmButton.addEventListener('click', () => {

        let validator = true;
        let inputs = [
            inputYear,
            inputMonth,
            inputDay,
        ];

        inputs.map(input => {
            if (input.value == '') {
                input.style.border = '.5vh solid #c52525';
                validator = validator && false;
            } else {
                input.style.border = '.5vh solid #d87e18';
            }
        })

        if (validator) {
            tableInfo[companyName].date = `${dateAddZeros(Math.abs(inputMonth.value))}/${dateAddZeros(Math.abs(inputDay.value))}/${dateAddZeros(Math.abs(inputYear.value))}`;
            closeInputContainer()
            avaribleButtons()
        } else {
            alert('Error.Missing Data')
        }

    })

    const cancelButton = html('button', {
        margin: '0vh 5vh 0vh 0vh',
    }, {
        class: 'buttonDocxManuallyInside',
        textContent: 'CANCEL'
    }, inputContainer);

    cancelButton.addEventListener('click', () => {
        if (tableInfo[companyName].date == undefined) {
            tableInfo[companyName].dateFormat = undefined;
        }
        closeInputContainer()
        avaribleButtons()
    })



}


const createDOCXmanuallyDeadLineSpanishFormatUI = (companyName) => {
    const inputContainer = createFormatUI(companyName, 'Spanish');
    tableInfo[companyName].dateFormat = 'spanish';

    let dateArr = [];

    if (tableInfo[companyName].date != undefined) {
        dateArr = fromDateToArr(tableInfo[companyName].date);
    }


    const inputDay = html('input', {
        margin: '0vh 0vh 0vh 3vh',
        width: '15vh',
    }, {
        class: 'inputDocxManuallyInside',
        placeholder: `DD`,
        value: (dateArr[2] != undefined) ? (dateArr[0]) : (``),
        type: `number`,
        min: 1,
        max: 31,
    }, inputContainer);


    inputDay.addEventListener('change', () => {
        const inputValue = parseInt(inputDay.value);
        if (inputValue < 1) inputDay.value = 1;
        if (inputValue > 31) inputDay.value = 31;
    })

    html('h3', {
        margin: '0vh 2vh 0vh 2vh',
        color: '#187953',
        fontSize: '4vh',
    }, {
        class: 'textDocxManuallyInside',
        textContent: `Dia`
    }, inputContainer);


    const inputMonth = html('input', {
        margin: '0vh 0vh 0vh 3vh',
        width: '15vh',
    }, {
        class: 'inputDocxManuallyInside',
        placeholder: `MM`,
        value: (dateArr[1] != undefined) ? (dateArr[1]) : (``),
        type: `number`,
        min: 1,
        max: 12,
    }, inputContainer);

    inputMonth.addEventListener('change', () => {
        const inputValue = parseInt(inputMonth.value);
        if (inputValue < 1) inputMonth.value = 1;
        if (inputValue > 12) inputMonth.value = 12;
    })




    html('h3', {
        margin: '0vh 2vh 0vh 2vh',
        color: '#187953',
        fontSize: '4vh',
    }, {
        class: 'textDocxManuallyInside',
        textContent: `Mes`
    }, inputContainer);



    const inputYear = html('input', {
        margin: '3vh 0vh 5vh 3vh',
        width: '15vh',
    }, {
        class: 'inputDocxManuallyInside',
        placeholder: `YYYY`,
        value: (dateArr[0] != undefined) ? (dateArr[2]) : (``),
        type: `number`,
        min: 1900,
        max: 2500,
    }, inputContainer);

    inputYear.addEventListener('change', () => {
        const inputValue = parseInt(inputYear.value);
        if (inputValue < 1900) inputYear.value = 1900;
        if (inputValue > 2500) inputYear.value = 2500;
    })

    html('h3', {
        margin: '0vh 2vh 0vh 2vh',
        color: '#187953',
        fontSize: '4vh',
    }, {
        class: 'textDocxManuallyInside',
        textContent: `Año`

    }, inputContainer);


    html('br', {}, {}, inputContainer)

    const backButton = html('button', {
        margin: '5vh 5vh 0vh 0vh',
    }, {
        class: 'buttonDocxManuallyInside',
        textContent: 'CHANGE FORMAT'
    }, inputContainer);

    backButton.addEventListener('click', () => {
        tableInfo[companyName].dateFormat = undefined;
        tableInfo[companyName].date = undefined;

        closeInputContainer()
        avaribleButtons()
        showDOCXmanuallyDeadLineMenu(companyName);
    })

    const confirmButton = html('button', {
        margin: '0vh 5vh 0vh 0vh',
    }, {
        class: 'buttonDocxManuallyInside',
        textContent: 'CONFIRM'
    }, inputContainer);

    confirmButton.addEventListener('click', () => {

        let validator = true;
        let inputs = [
            inputYear,
            inputMonth,
            inputDay,
        ];

        inputs.map(input => {
            if (input.value == '') {
                input.style.border = '.5vh solid #c52525';
                validator = validator && false;
            } else {
                input.style.border = '.5vh solid #d87e18';
            }
        })

        if (validator) {
            tableInfo[companyName].date = `${dateAddZeros(Math.abs(inputDay.value))}/${dateAddZeros(Math.abs(inputMonth.value))}/${dateAddZeros(Math.abs(inputYear.value))}`;
            closeInputContainer()
            avaribleButtons()
        } else {
            alert('Error.Missing Data')
        }

    })

    const cancelButton = html('button', {
        margin: '0vh 5vh 0vh 0vh',
    }, {
        class: 'buttonDocxManuallyInside',
        textContent: 'CANCEL'
    }, inputContainer);

    cancelButton.addEventListener('click', () => {

        if (tableInfo[companyName].date == undefined) {
            tableInfo[companyName].dateFormat = undefined;
        }
        closeInputContainer()
        avaribleButtons()
    })



}

