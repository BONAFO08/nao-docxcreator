


const validateTextInfoTable = (data) => {
    const response = {
        bool: '',
        data: ''
    };

    if (data.trim() != '') {
        response.bool = true;
        response.data = data.trim();
    } else {
        response.bool = false;
    }
    return response;
}

const validateNumberInfoTable = (data) => {
    const response = {
        bool: '',
        data: ''
    };

    if (!isNaN(parseInt(data))) {
        response.bool = true;
        response.data = parseInt(data);
    } else {
        response.bool = false;
    }
    return response;

}

const validateEmailInfoTable = (data) => {
    const response = {
        bool: '',
        data: ''
    };

    if (data.trim() != '' && data.includes('@')) {
        response.bool = true;
        response.data = data.trim();
    } else {
        response.bool = false;
    }
    return response;
}



const showDOCXmanuallyAddProductData = (height, container, companyName) => {
    const InfoContainer = html('div', {
        margin: `-50vh 0vh 0vh -10vh`,
        width: `70vh`,
        height: `${(height) + (15 * tableBasicInfoOriginal.length)}vh`,
    }, { id: 'inputContainer', class: 'infoContainer' }, container)


    tableBasicInfoOriginal.map(tableBasic => {
        html('h3', { margin: '2vh 0vh 1vh 0vh', fontSize: '3.5vh', color: '#187953' }, { class: 'textDocxManuallyInside', textContent: tableBasic.name.toUpperCase() }, InfoContainer)
        html('br', {}, {}, InfoContainer)
        html('input', { margin: '1vh 0vh 3vh 0vh', width: '45vh', fontSize: '3.5vh', border: ".5vh solid #187953" }, { class: 'inputDocxManuallyInside', name: tableBasic.name, placeholder: tableBasic.name.toUpperCase(), type: tableBasic.type }, InfoContainer)
        html('br', {}, {}, InfoContainer)
    })

    const responseText = html('h3', { margin: '2vh 0vh 0vh 0vh', opacity: 0 }, { class: 'responseTextDocxManuallyInside', textContent: 'NOTHING' }, InfoContainer)
    html('br', {}, {}, InfoContainer)


    const buttonSendCompanyName = html('button', { margin: '4vh 4vh 0vh 0vh' }, { class: 'buttonDocxManuallyInside', textContent: `SAVE` }, InfoContainer)

    buttonSendCompanyName.addEventListener('click', () => {
        const inputs = Array.prototype.slice.call(document.getElementsByClassName('inputDocxManuallyInside'));

        let dataContainer = {
            data: {},
            bool: true
        }
        inputs.map(input => {
            const tableBasicInformation = tableBasicInfoOriginal.filter(infoTable => infoTable.name == input.name)[0];
            let dataValidated;

            switch (tableBasicInformation.type) {
                case 'text':
                    dataValidated = validateTextInfoTable(input.value);
                    break;
                case 'number':
                    dataValidated = validateNumberInfoTable(input.value);
                    break;
                case 'email':
                    dataValidated = validateEmailInfoTable(input.value);
                    break;
                default: console.error('Error in Manager Data Table')
                    dataValidated = { bool: false };
                    break;

            }

            dataContainer.bool = dataContainer.bool && dataValidated.bool
            if (dataValidated.bool) {
                dataContainer.data[input.name] = dataValidated.data
                input.style.border = ".5vh solid #187953";
            } else {
                input.style.border = ".5vh solid #c02e2e";
            }
        })


        if (dataContainer.bool) {
            dataContainer['companyName'] = companyName;
            tableInfo[companyName].push(dataContainer.data);
            console.log(tableInfo);
            closeInputContainer()
            closeInputContainer()
            avaribleButtons()
            showDataTableDOCXmanually(companyName);
        } else {
            responseText.textContent = 'Some data is invalid!';
            responseText.style.opacity = 1;

        }



    })





    const buttonClearCompanyName = html('button', { margin: '4vh 0vh 0vh 4vh' }, { class: 'buttonDocxManuallyInside', textContent: `CLEAR` }, InfoContainer)
    buttonClearCompanyName.addEventListener('click', () => {
        const inputs = Array.prototype.slice.call(document.getElementsByClassName('inputDocxManuallyInside'));
        inputs.map(input => input.value = '')
    })

}


const showDOCXmanuallyProductInfo = (height, container, productData) => {

    const InfoContainer = html('div', {
        margin: `-50vh 0vh 0vh -10vh`,
        width: `70vh`,
        height: `${height}vh`,
    }, { id: 'inputContainer', class: 'infoContainer' }, container)

    if (productData != undefined) {

        tableBasicInfoOriginal.map(tableBasic => {

            const text = html('h3', { margin: '2vh 0vh 3vh 0vh' }, { class: 'textCompanyNames' }, InfoContainer);
            text.innerHTML = `${tableBasic.name.toUpperCase()}: <span style = 'color : #187953;'> ${productData[tableBasic.name]} </span>`
            html('br', {}, {}, InfoContainer)

        })
    }
}

const showDataTableDOCXmanually = (companyName) => {
    disableButtons();
    const companyInfoTable = structuredClone(tableInfo);


    const divDataContainerCompanyName = document.getElementById(`CompanyNameDivContainer${companyName}`)
    const heightContainer = 70 + (20 * companyInfoTable[companyName].length);

    const container = html('div', {
        margin: `-50vh 0vh 0vh -85vh`,
        width: `70vh`,
        height: `${heightContainer}vh`,
    }, { id: 'inputContainer', }, divDataContainerCompanyName)


    html('h3', { margin: '3vh 0vh 10vh 2vh' }, { class: 'textCompanyNames', id: `CompanyNameTextContainer${companyName}`, textContent: companyName }, container)
    html('br', {}, {}, container)



    showDOCXmanuallyAddProductData(heightContainer,
        divDataContainerCompanyName, companyName)

    companyInfoTable[companyName].map(rowInfo => {
        const rowInfoContainer = html('div', {}, {}, container)
        const rowInfoButton = html('button', { margin: '1vh 0vh 3vh 0vh' }, { class: 'buttonDocxManuallyInside', textContent: `${rowInfo.product}` }, rowInfoContainer)
        html('br', {}, {}, container)


        rowInfoButton.addEventListener('click', () => {
            const internalContainers = Array.prototype.slice.call(document.getElementsByClassName('infoContainer'));
            internalContainers.map(infoContainer => infoContainer.remove())
            showDOCXmanuallyProductInfo(
                heightContainer,
                divDataContainerCompanyName,
                rowInfo)
        })


    })

    const buttonAddCompanyData = html('button', { margin: '4vh 0vh 10vh 0vh', padding: '2vh 5vh 2vh 5vh' }, { class: 'buttonDocxManuallyInside', textContent: `ADD PRODUCT` }, container)

    buttonAddCompanyData.addEventListener('click', () => {
        const internalContainers = Array.prototype.slice.call(document.getElementsByClassName('infoContainer'));
        internalContainers.map(infoContainer => infoContainer.remove())
        showDOCXmanuallyAddProductData(heightContainer,
            divDataContainerCompanyName, companyName)
    })

    html('br', {}, {}, container)

    const buttonSendCompanyName = html('button', { margin: '4vh 0vh 0vh 0vh' }, { class: 'buttonDocxManuallyInside', textContent: `OK!` }, container)
    const buttonCancelCompanyName = html('button', { margin: '0vh 0vh 0vh 10vh' }, { class: 'buttonDocxManuallyInside', textContent: `CANCEL` }, container)

    buttonCancelCompanyName.addEventListener('click', () => {
        closeInputContainer()
        closeInputContainer()
        avaribleButtons()
    })

}