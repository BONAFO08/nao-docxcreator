
const validatingMinimuData = () => {
    if (companyName.length <= 0) {
        return false
    }

    if (presentationText == '') {
        return false
    }
    return true
}


const toTextToArr = (text) => {
    let breaker = 100;
    const textArr = [];
    let indexEND;
    let objectAux;

    for (let i = 0; i < breaker; i++) {
        indexEND = text.indexOf('{n}');
        objectAux = text.substring(0, indexEND + 3);
        text = text.replace(objectAux, '');
        objectAux = objectAux.replace('{n}', '');
        objectAux = objectAux.replace(/\n/, '');
        if (text.trim() == '') {
            textArr.push(objectAux);
            i = 100000;
        } else {
            textArr.push(objectAux);
        }
    }

    return textArr;
}



const createFirstContainerSendData = (divDataContainer, heightContainer) => {
    const inputContainer = html('div', {
        margin: `0vh 0vh 0vh 0vh`,
        width: `60vh`,
        height: `${heightContainer}vh`,
        zIndex: 1000,
    }, { id: 'inputContainer', class: 'first-container' }, divDataContainer)




    html('h3', { margin: '4vh 0vh 0vh 0vh' }, { class: 'textDocxManuallyInside', textContent: `Please verify the data` }, inputContainer)
    html('br', {}, {}, inputContainer)


    const presentationTextButton = html('button', { margin: '4vh 0vh 3vh 0vh' }, { class: 'buttonDocxManuallyInside', textContent: `PRESENTATION TEXT` }, inputContainer)
    html('br', {}, {}, inputContainer)

    presentationTextButton.addEventListener('click', () => {
        const secondContainer = document.getElementsByClassName('second-container')[0];
        clearDiv(secondContainer);
        secondContainer.style.textAlign = 'left';
        presentationTextArr = toTextToArr(presentationText);
        html('br', {}, {}, secondContainer)
        html('br', {}, {}, secondContainer)
        presentationTextArr.map(textline => {
            html('h3', { margin: '2vh 0vh 0vh 1vh', textAlign: 'left' }, { class: 'textDocxManuallyInside', textContent: textline.trim() }, secondContainer)
            html('br', {}, {}, secondContainer)
        })
    })


    html('button', { margin: '4vh 0vh 3vh 0vh' }, { class: 'buttonDocxManuallyInside', textContent: `PERSONAL DATA` }, inputContainer)
    html('br', {}, {}, inputContainer)

    companyName.map(name => {

        const button = html('button',
            { margin: '4vh 0vh 0vh 0vh', padding: '2vh 5vh 2vh 5vh' },
            { class: 'buttonDocxManuallyInside', textContent: name + ' DATA' },
            inputContainer)

        html('br', {}, {}, inputContainer)


    })

    const sendButton = html('button',
        { margin: '10vh 0vh 0vh 0vh', padding: '2vh 5vh 2vh 5vh' },
        { class: 'buttonDocxManuallyInside', textContent: 'SEND DATA' },
        inputContainer)

    addConfirmHover(sendButton);

    html('br', {}, {}, inputContainer)

    const cancelButton = html('button',
        { margin: '3vh 0vh 0vh 0vh', padding: '2vh 5vh 2vh 5vh' },
        { class: 'buttonDocxManuallyInside', textContent: 'CLOSE' },
        inputContainer)

    addCancelHover(cancelButton);

    cancelButton.addEventListener('click', () => {
        closeInputContainer()
        closeInputContainer()
        closeInputContainer()

        avaribleButtons()
    })

    html('br', {}, {}, inputContainer)

}


const createSecondContainerSendData = (divDataContainer, heightContainer) => {
    const secondcontainer = html('div', {
        margin: `0vh 0vh 0vh 65vh`,
        width: `65vh`,
        height: `${heightContainer}vh`,
        zIndex: 1000,
    }, { id: 'inputContainer', class: 'second-container' }, divDataContainer)


    html('h3', { margin: '4vh 0vh 0vh 0vh' }, { class: 'textDocxManuallyInside', textContent: `Please verify the data` }, secondcontainer)
    html('br', {}, {}, secondcontainer)


    html('h3', { margin: '4vh 0vh 0vh 0vh' }, { class: 'textDocxManuallyInside', textContent: `Please verify the data` }, secondcontainer)
    html('br', {}, {}, secondcontainer)

    html('h3', { margin: '4vh 0vh 0vh 0vh' }, { class: 'textDocxManuallyInside', textContent: `Please verify the data` }, secondcontainer)
    html('br', {}, {}, secondcontainer)

    html('h3', { margin: '4vh 0vh 0vh 0vh' }, { class: 'textDocxManuallyInside', textContent: `Please verify the data` }, secondcontainer)
    html('br', {}, {}, secondcontainer)


}


const createThirdContainerSendData = (divDataContainer, heightContainer) => {
    const thirthcontainer = html('div', {
        margin: `0vh 0vh 0vh 135vh`,
        width: `65vh`,
        height: `${heightContainer}vh`,
        zIndex: 1000,
    }, { id: 'inputContainer', class: 'third-container' }, divDataContainer)
}