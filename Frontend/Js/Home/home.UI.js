validateUserToken()

const homeContainer = html('div', {}, { id: 'homeContainer' });

const docxManuallyURL = `${baseURL}/docx.manually.html`;
html('a', { margin: '6vh 0vh 0vh 12vh' }, { class: 'buttonsHome', textContent: 'CREATE DOCX (UPLOAD DATA MANUALLY)', href : `${docxManuallyURL}`}, homeContainer)
html('br', {}, {}, homeContainer)

const docxTeseractURL = `${baseURL}/docx.teseract.html`;
html('a', { margin: '6vh 0vh 0vh 12vh' }, { class: 'buttonsHome', textContent: 'CREATE DOCX (USE TESERACT.JS)' , href : `${docxTeseractURL}`}, homeContainer)
html('br', {}, {}, homeContainer)

const userManagerURL = `${baseURL}/userManagerData.html`;
html('a', { margin: '6vh 0vh 0vh 12vh' }, { class: 'buttonsHome', textContent: 'UPDATE PERSONAL DATA' , href : `${userManagerURL}`}, homeContainer)
html('br', {}, {}, homeContainer)

const Logout =()=>{
     sessionStorage.clear()
     window.location.reload()
}

const logOutButton = html('button', { margin: '6vh 0vh 0vh 12vh' }, { class: 'buttonsHome', textContent: 'LOGOUT' }, homeContainer)

logOutButton.addEventListener('click',()=>{
    Logout()
})

html('br', {}, {}, homeContainer)