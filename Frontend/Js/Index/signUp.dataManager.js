const SIgnUp = async (userData) => {
    let resp;
    let myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    let requestOptions = {
        method: 'POST',
        body: JSON.stringify(userData),
        headers: myHeaders,
        redirect: 'follow'
    };

    await fetch(`${backendURL}/user/signUp`, requestOptions)
        .then(response => response.json())
        .then(result => resp = result)
        .catch(error => console.log('error', error));
    return resp;

}


const passwordConfirmation = (password, repassword) => {
    if (password === repassword) {
        return true
    } else {
        return false
    }
}


const restoreInputsBorderColorSignUp = () => {
    const inputs = Array.prototype.slice.call(document.getElementsByClassName('inputSignUp'));
    inputs.map(input => input.style.border = '.5vh solid #d87e18')
}

const highlightErrorInputsSignUp = (inputsEmpties) => {
    const inputs = document.getElementsByClassName('inputSignUp');
    inputsEmpties.map(inputName => inputs[inputName].style.border = '.5vh solid #c52525')
}

const validateInputDataSignUp = () => {
    const response = {
        boolean: true,
        inputsEmpties: []
    };
    const inputs = Array.prototype.slice.call(document.getElementsByClassName('inputSignUp'));
    inputs.map(input => {
        const inputData = input.value.trim().toString();
        if ((inputData.length != 0)) {
            if (input.name == 'dni') {
                (parseInt(inputData) == NaN)
                    ? (response.boolean = response.boolean && false,
                        response.inputsEmpties.push('dni'))
                    : (response.boolean = response.boolean && true);
            } else if (input.name == 'email') {
                (inputData.includes('@') && inputData.includes('.com'))
                    ? (response.boolean = response.boolean && true)
                    : (response.boolean = response.boolean && false,
                        response.inputsEmpties.push('email'));
            }
        } else {
            response.boolean = response.boolean && false;
            response.inputsEmpties.push(input.name)
        }


    })
    return response
}

const getDataInputsSignUp = async () => {

    restoreInputsBorderColorSignUp();


    const response = {};

    const inputs = document.getElementsByClassName('inputSignUp');
    const userData = {
        username: inputs.username.value.trim(),
        name: inputs.name.value.trim(),
        surname: inputs.surname.value.trim(),
        email: inputs.email.value.trim(),
        companyId: inputs.companyId.value.trim(),
        dni: (inputs.dni.value.toString()).trim(),
        password: inputs.password.value.trim(),
        repassword: inputs.repassword.value.trim(),
    };



    if (!validateInputDataSignUp(userData).boolean) {
        highlightErrorInputsSignUp(validateInputDataSignUp(userData).inputsEmpties)
        response.msj = 'Missing Data : ';
        validateInputDataSignUp(userData).inputsEmpties.map(ele => response.msj += ele + ', ')
        response.boolean = false;
        return response
    }

    if (!passwordConfirmation(userData.password, userData.repassword)) {
        response.msj = `Passwords don't match`;
        response.boolean = false;
        inputs.password.style.border = '.5vh solid #c52525';
        inputs.repassword.style.border = '.5vh solid #c52525';
        return response
    }


    const serverResponse = await SIgnUp(userData)

    response.msj = serverResponse.msj;
    (serverResponse.status == 200)
        ? (response.msj = 'Account created, redirecting...',
            response.boolean = true)
        : (response.boolean = false);
    return response;

}

const clearInputsSignUp =()=>{
    const inputs = Array.prototype.slice.call(document.getElementsByClassName('inputSignUp'));
    inputs.map(input => input.value = '')
}