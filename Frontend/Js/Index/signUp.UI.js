const createSignUpUI = () => {
    const signUpContainer = html('div', {}, { id: 'signUpContainer' });

    html('h3', {}, { id: 'signUpTitle', textContent: 'CREATE YOUR ACCOUNT!' }, signUpContainer)

    html('br', {}, {}, signUpContainer)


    html('h3', { margin: '2vh 0vh 0vh 22vh' }, { class: 'textSignUp', textContent: 'NAME' }, signUpContainer)
    html('h3', { margin: '2vh 0vh 0vh 60vh' }, { class: 'textSignUp', textContent: 'SURNAME' }, signUpContainer)


    html('br', {}, {}, signUpContainer)


    html('input', { margin: '0vh 0vh 0vh 12vh' }, { class: 'inputSignUp', name: 'name' }, signUpContainer)
    html('input', { margin: '0vh 0vh 0vh 12vh' }, { class: 'inputSignUp', name: 'surname' }, signUpContainer)

    html('br', {}, {}, signUpContainer)


    html('h3', { margin: '2vh 0vh 0vh 22vh' }, { class: 'textSignUp', textContent: 'PASSWORD' }, signUpContainer)
    html('h3', { margin: '2vh 0vh 0vh 40vh' }, { class: 'textSignUp', textContent: 'REPEAT PASSWORD' }, signUpContainer)


    html('br', {}, {}, signUpContainer)


    html('input', { margin: '0vh 0vh 0vh 12vh' }, { class: 'inputSignUp', type: 'password', name: 'password' }, signUpContainer)
    html('input', { margin: '0vh 0vh 0vh 12vh' }, { class: 'inputSignUp', type: 'password', name: 'repassword' }, signUpContainer)


    html('br', {}, {}, signUpContainer)


    html('h3', { margin: '2vh 0vh 0vh 22vh' }, { class: 'textSignUp', textContent: 'USERNAME' }, signUpContainer)
    html('h3', { margin: '2vh 0vh 0vh 50vh' }, { class: 'textSignUp', textContent: 'EMAIL' }, signUpContainer)


    html('br', {}, {}, signUpContainer)


    html('input', { margin: '0vh 0vh 0vh 12vh' }, { class: 'inputSignUp', name: 'username' }, signUpContainer)
    html('input', { margin: '0vh 0vh 0vh 12vh' }, { class: 'inputSignUp', type: 'email', name: 'email' }, signUpContainer)


    html('br', {}, {}, signUpContainer)


    html('h3', { margin: '2vh 0vh 0vh 22vh' }, { class: 'textSignUp', textContent: 'COMPANY ID' }, signUpContainer)
    html('h3', { margin: '2vh 0vh 0vh 40vh' }, { class: 'textSignUp', textContent: 'PERSONAL ID' }, signUpContainer)


    html('br', {}, {}, signUpContainer)


    html('input', { margin: '0vh 0vh 0vh 12vh' }, { class: 'inputSignUp', name: 'companyId' }, signUpContainer)
    html('input', { margin: '0vh 0vh 0vh 12vh' }, { class: 'inputSignUp', type: 'number', name: 'dni' }, signUpContainer)

    html('br', {}, {}, signUpContainer)


    const textResponseSignUp = html('h3', { margin: '4vh 0vh 0vh 22vh' }, { class: 'textResponseSignUp', textContent: 'NOTHING' }, signUpContainer)


    html('br', {}, {}, signUpContainer)


    const sendDataSignUp = html('button', { margin: '6vh 0vh 0vh 12vh' }, { class: 'buttonSignUp', textContent: 'SEND' }, signUpContainer)

    sendDataSignUp.addEventListener('click', async () => {
        const response = await getDataInputsSignUp()
        textResponseSignUp.style.opacity = 1;
        textResponseSignUp.textContent = response.msj;

        (!response.boolean)
            ? (textResponseSignUp.style.color = '#c52525')
            : (textResponseSignUp.style.color = '#d87e18');

        if (response.boolean) {
            setTimeout(() => {
                changeOfDivAnimation('signUp')
                setTimeout(() => {
                    signUpContainer.remove()
                    showLogInDiv()
                }, 300)
            }, 2000);
        }
    })

    const clearInputsButtonSignUp = html('button', { margin: '6vh 0vh 0vh 12vh' }, { class: 'buttonSignUp', textContent: 'CLEAR INFO' }, signUpContainer)

    clearInputsButtonSignUp.addEventListener('click', () => {
        clearInputsSignUp()
    })


    const toLoginButton = html('button', { margin: '6vh 0vh 0vh 12vh' }, { class: 'buttonSignUp', textContent: 'I HAVE AN ACCOUNT' }, signUpContainer)

    toLoginButton.addEventListener('click', () => {
        changeOfDivAnimation('signUp')
        setTimeout(() => {
            signUpContainer.remove()
            showLogInDiv()
        }, 300)
    })


    setTimeout(() => {
        signUpContainer.style.opacity = '1';
    }, 100);



}
