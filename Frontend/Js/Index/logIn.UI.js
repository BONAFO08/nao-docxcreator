const createLogInUI = () => {
    html('h1', {}, { id: 'titleIndex', textContent: 'Document Generation System ' });

    const logInContainer = html('div', {}, { id: 'logInContainer' });

    html('h3', {}, { id: 'logInTitle', textContent: 'CREATE YOUR ACCOUNT!' }, logInContainer)

    html('br', {}, {}, logInContainer)


    html('h3', { margin: '4vh 0vh 0vh 0vh' }, { class: 'textLogIn', textContent: 'USERNAME OR EMAIL' }, logInContainer)
    html('br', {}, {}, logInContainer)
    html('input', { margin: '0vh 0vh 0vh 0vh' }, { class: 'inputLogIn', name: 'username' }, logInContainer)

    html('br', {}, {}, logInContainer)


    html('h3', { margin: '5vh 0vh 0vh 0vh' }, { class: 'textLogIn', textContent: 'PASSWORD' }, logInContainer)
    html('br', {}, {}, logInContainer)
    html('input', { margin: '0vh 0vh 0vh 0vh' }, { class: 'inputLogIn', type: 'password', name: 'password' }, logInContainer)

    html('br', {}, {}, logInContainer)

    const textResponseLogIn = html('h3', { margin: '4vh 0vh 0vh 0vh' }, { class: 'textResponseLogIn', textContent: 'NOTHING' }, logInContainer)

    html('br', {}, {}, logInContainer)

    const sendDataLogIn = html('button', { margin: '4vh 0vh 0vh 0vh' }, { class: 'buttonLogIn', textContent: 'SEND' }, logInContainer)

    sendDataLogIn.addEventListener('click', async () => {
        const response = await getDataInputsLogIn()
        textResponseLogIn.style.opacity = 1;
        textResponseLogIn.textContent = response.msj;


        (!response.boolean) 
        ?(textResponseLogIn.style.color = '#c52525') 
        :(textResponseLogIn.style.color = '#d87e18');


        if (response.boolean) {
            setTimeout(() => {
                changeOfDivAnimation('signUp')
                setTimeout(() => {
                    signUpContainer.remove()
                    showLogInDiv()
                }, 300)
            }, 2000);
        }
    })

    const clearInputsButtonLogIn = html('button', { margin: '4vh 0vh 0vh 5vh' }, { class: 'buttonLogIn', textContent: 'CLEAR INFO' }, logInContainer)

    clearInputsButtonLogIn.addEventListener('click', () => {
        clearInputsLogIn()
    })



    html('br', {}, {}, logInContainer)

    const toSignUpButton = html('button', { margin: '3vh 0vh 0vh 0vh' }, { class: 'buttonLogIn', textContent: `I DON'T HAVE AN ACCOUNT` }, logInContainer)

    toSignUpButton.addEventListener('click', () => {
        changeOfDivAnimation('logIn')
        setTimeout(() => {
            logInContainer.remove()
            document.getElementById('titleIndex').remove()
            showSignUpDiv()
        }, 300)
    })

    setTimeout(() => {
        logInContainer.style.opacity = '1';
    }, 100);
}