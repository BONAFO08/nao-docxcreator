const LogIn = async (userData) => {
    let resp;
    let myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    let requestOptions = {
        method: 'POST',
        body: JSON.stringify(userData),
        headers: myHeaders,
        redirect: 'follow'
    };

    await fetch(`${backendURL}/user/logIn`, requestOptions)
        .then(response => response.json())
        .then(result => resp = result)
        .catch(error => console.log('error', error));
    return resp;

}


const restoreInputsBorderColorLogIn = () => {
    const inputs = Array.prototype.slice.call(document.getElementsByClassName('inputLogIn'));
    inputs.map(input => input.style.border = '.5vh solid #d87e18')
}

const highlightErrorInputsLogIn = (inputsEmpties) => {
    const inputs = document.getElementsByClassName('inputLogIn');
    inputsEmpties.map(inputName => inputs[inputName].style.border = '.5vh solid #c52525')
}

const validateInputDataLogIn = () => {
    const response = {
        boolean: true,
        inputsEmpties: []
    };
    const inputs = Array.prototype.slice.call(document.getElementsByClassName('inputLogIn'));
    inputs.map(input => {
        const inputData = input.value.trim().toString();
        if ((inputData.length == 0)) {
            response.boolean = response.boolean && false;
            response.inputsEmpties.push(input.name)
        }
    })
    return response
}


const getDataInputsLogIn = async () => {

    restoreInputsBorderColorLogIn();

    const response = {};

    const inputs = document.getElementsByClassName('inputLogIn');
    const userData = {
        username: inputs.username.value.trim(),
        password: inputs.password.value.trim(),
    };

    if (!validateInputDataLogIn(userData).boolean) {
        highlightErrorInputsLogIn(validateInputDataLogIn(userData).inputsEmpties)
        response.msj = 'Missing Data : ';
        validateInputDataLogIn(userData).inputsEmpties.map(ele => response.msj += ele + ', ')
        response.boolean = false;
        return response
    }

    const serverResponse = await LogIn(userData)
    response.msj = serverResponse.msj;
        (serverResponse.status == 200)
        ? (sessionStorage.setItem('token', serverResponse.token),
            window.location.href = homeURL)
        : (response.boolean = false);
    return response;

}


const clearInputsLogIn = () => {
    const inputs = Array.prototype.slice.call(document.getElementsByClassName('inputLogIn'));
    inputs.map(input => input.value = '')
}