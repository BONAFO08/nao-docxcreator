const createIndexUI = () => {
    html('h1', {}, { id: 'titleIndex', textContent: 'Document Generation System ' });


    const indexContainer = html('div', {}, { id: 'indexContainer' });


    html('h3', {}, { class: 'textIndex', textContent: 'WELCOME!' }, indexContainer)

    const signUpButton = html('button', {
        margin: '8vh 0vh 0vh 0vh'
    }, { class: 'buttonIndex', textContent: 'CREATE ACCOUNT' }, indexContainer)

    html('br', {}, {}, indexContainer)


    const logInButton = html('button', {
        margin: '8vh 0vh 0vh 0vh'
    }, { class: 'buttonIndex', textContent: 'LOG IN' }, indexContainer)

    signUpButton.addEventListener('click', () => {
        changeOfDivAnimation('index')
        setTimeout(() => {
            indexContainer.remove()
            document.getElementById('titleIndex').remove()
            showSignUpDiv()
        }, 300)
    })
    logInButton.addEventListener('click', () => {
        changeOfDivAnimation('index')
        setTimeout(() => {
            indexContainer.remove()
            document.getElementById('titleIndex').remove()
            showLogInDiv()
        }, 300)
    })

}

createIndexUI()