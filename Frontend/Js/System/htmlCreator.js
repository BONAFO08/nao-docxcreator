const html = (htmlType, style = {
    display: '',
    position: '',
    margin: '',
    padding: '',
    color: '',
    fontSize: '',
    border: '',
    borderRadius: '',
    background: '',
    backgroundColor: '',
    backgroundImage: '',
    backgroundSize: '',
    backgroundRepeat: '',
    backgroundPosition: '',
    textAlign: '',
    textShadow: '',
    zIndex: '',
    width: '',
    visibility: '',
    transition: '',
    transform: '',
    textDecoration: '',
    resize: '',
    overflow: '',
    opacity: '',
    maxHeight: '',
    maxWidth: '',
    minHeight: '',
    minWidth: '',
    height: '',
    font: '', // 	Sets or returns fontStyle, fontVariant, fontWeight, fontSize, lineHeight, and fontFamily in one declaration
    fontStyle: ' ',
    fontVariant: ' ',
    fontWeight: ' ',
    fontSize: ' ',
    lineHeight: ' ',
    fontFamily: ' ',
    boxShadow: '',
    cursor: '',
}, atributes = {
    src: '',
    loop: false,
    id: '',
    class: '',
    textContent: '',
    href: ''
}, div = document.getElementById('root')) => {


    const element = document.createElement(htmlType);
    const atributeKey = Object.keys(atributes);
    const atributeValue = Object.values(atributes);
    atributeKey.map((key, index) => {
        if (!key.includes('text') && !key.includes('Text')) {
            return element.setAttribute(key, atributeValue[index])
        } else {
            return element[key] = atributeValue[index]
        }
    })
    const styleKey = Object.keys(style);
    const styleValue = Object.values(style);
    styleKey.map((key, index) => element.style[key] = styleValue[index])
    div.appendChild(element)
    return element
}

const clearDiv = (div) => {
    const nodesNumber = div.childNodes.length
    for (let i = 0; i < nodesNumber; i++) {
        div.childNodes[0].remove()
    }
}
