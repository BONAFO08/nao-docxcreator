
document.title = 'DGS';

const baseURL = 'file:///C:/Users/Asus/Desktop/MINI%20PROYECTS/Nueva%20carpeta/Frontend/Views';

const frontURL = `${baseURL}/index.html`;
const homeURL = `${baseURL}/home.html`;



const backendURL = 'http://localhost:3600';




const validator = async () => {
    let resp;
    let myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    myHeaders.append("Authorization", window.sessionStorage.getItem('token'));

    let requestOptions = {
        method: 'GET',
        headers: myHeaders,
        redirect: 'follow'
    };


    await fetch(`${backendURL}/user/validateToken`, requestOptions)
        .then(response => resp = response.json())
        .catch(error => console.log('error', error));
    return resp;
}


const validateUserToken = () => {
 const returnPage = frontURL;
 
    try {
        if (window.sessionStorage.getItem('token') == undefined || window.sessionStorage.getItem('token') == null) {
            window.location.href = returnPage;
        } else {
            const response = validator()
                .then(response => {
                    if (response.status !== 200) {
                        window.location.href = returnPage;
                    }
                })
                .catch(error => {
                    console.log(error);
                    window.location.href = returnPage;
                });
        }
    } catch (error) {
        console.log(error);
        window.location.href = returnPage;
    }
}

