const { createUser, validateUser, validateToken, searchDataUser, updateDataUser } = require("../Controllers/user.controllers");
const colors = require('colors');


//Create a new user
const signUp = async (req, res) => {
    const response = await createUser(req.body);
    res.status(response.status).json({ msj: response.text, status: response.status })
    if (response.status == 200) {
        console.log(`[${new Date}] User Creator : A new user has born! // USER_ID: ${response.text._id}`.green);

    } else {
        console.log(`[${new Date}] User Creator : Error! // Error: ${response.text}`.red);
    }
}


const logIn = async (req, res) => {
    const response = await validateUser(req.body);
    if (response.boolean != false) {
        res.status(200).json({ token: response.token, status: 200 });
        console.log(`[${new Date}] User Validator : User validated correctly! //`.green);
    } else {
        res.status(403).json({ msj: response.text, status: response.status })
        console.log(`[${new Date}] User Validator  : Error! // Error: ${response.text}`.red);
    }
}

const validateTokenMiddle = (req, res) => {
    const desToken = validateToken(req.headers.authorization);
    if (desToken != false) {
        res.status(200).json({ status: 200 })
    } else {
        res.status(403).json({ status: 403 })
    }
}

const getUserDataMiddle = async (req, res) => {
    const desToken = validateToken(req.headers.authorization);
    if (desToken != false) {
        const response = await searchDataUser(desToken._id)
        if (response.status = 200) {
            const userData = {
                username: response.txt.username,
                name: response.txt.name,
                surname: response.txt.surname,
                email: response.txt.email,
                companyId: response.txt.companyId,
                dni: response.txt.dni,
            }
            console.log(`[${new Date}] User Validator : Sending Data of "${response.txt.username}" //`.green);
            res.status(response.status).json({ msj: userData, status: response.status });
        } else {
            console.log(`[${new Date}] User Validator : ${response.txt}//`.red);
            res.status(response.status).json({ msj: response.txt, status: response.status });
        }
    } else {
        res.status(403).json({ msj: 'Token invalid!', status: 403 })
        console.log(`[${new Date}] User Validator  : Error! // Error: ${'Token invalid!'}`.red);
    }
}

const saveNewUserDataMiddle = async (req, res) => {
    const desToken = validateToken(req.headers.authorization);
    if (desToken != false) {
        const response = await updateDataUser(req.body, desToken._id)

        res.status(response.status).json({ msj: response.txt, status: response.status });

        if (response.status == 200) {
            console.log(`[${new Date}] ${response.txt} //`.green);
        } else {
            console.log(`[${new Date}] User Updater : ${response.txt}//`.red);
        }
    } else {
        res.status(403).json({ msj: 'Token invalid!', status: 403 })
        console.log(`[${new Date}] User Validator  : Error! // Error: ${'Token invalid!'}`.red);
    }
}

module.exports = {
    signUp,
    logIn,
    validateTokenMiddle,
    getUserDataMiddle,
    saveNewUserDataMiddle
}