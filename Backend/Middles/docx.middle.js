const { Packer } = require('docx');
const fs = require("fs");
const { createfinalDocument, createDirectoryName } = require('../Controllers/docx.document.js');
const { validateToken, searchDataUser } = require('../Controllers/user.controllers.js');
const colors = require('colors');

const createDocumentsMiddle = async (req, res) => {

    const desToken = validateToken(req.headers.authorization);

    if (desToken != false) {
        const userValidator = await searchDataUser(desToken._id);
        if (userValidator.status == 200) {
            const userData = userValidator.txt;
            const baseText = `Por medio de este documento, informo a la compañia //companyName// de los costos que deberan proporcionar a la compañia Atlas para la fecha //date// los siguientes materiales.//`

            const dateSelected = '10/05/2001'

            const companyName = 'Atlas Inc.';

            const extraData = {
                dateSelected: dateSelected,
                companyName: companyName,
            }
            const tableInfo =
            {
                [companyName]: [{

                    product: 'Glass for windows',
                    quantity: { quant: 250, unit: 'unit' },
                    CU: { yen: '¥200', us: '(U$ 1.5)' },
                    CT: { yen: '¥50.000', us: '(U$ 375)' },
                    cert: 'Yes',
                    annot: 'El vidrio tiene un espesor de 1.5cm y una impureza de 10% ,El vidrio tiene un espesor de 1.5cm y una impureza de 10% ,El vidrio tiene un espesor de 1.5cm y una impureza de 10%',
                },
                {
                    product: 'Doors Indoors',
                    quantity: { quant: 15000, unit: 'unit' },
                    CU: { yen: '¥400', us: '(U$ 40.2)' },
                    CT: { yen: '¥2.000.000', us: '(U$ 100.075)' },
                    cert: 'No',
                    annot: 'El vidrio tiene un espesor de 1.5cm y una impureza de 10% ,El vidrio tiene un espesor de 1.5cm y una impureza de 10% ,El vidrio tiene un espesor de 1.5cm y una impureza de 10%'
                },
                {
                    product: 'Cemnet',
                    quantity: { quant: 150.000, unit: 'KG' },
                    CU: { yen: '¥40.000', us: '(U$ 40.2)' },
                    CT: { yen: '¥500.000.000', us: '(U$ 1.000.075)' },
                    cert: 'No',
                    annot: 'El vidrio tiene un espesor de 1.5cm y una impureza de 10% ,El vidrio tiene un espesor de 1.5cm y una impureza de 10% ,El vidrio tiene un espesor de 1.5cm y una impureza de 10%'
                },]
            }





            const dir = `./Documents/${createDirectoryName()}`;

            let validatorDir = '';
            try {
                await fs.promises.readdir(dir);
                validatorDir = true
            } catch (error) {
                validatorDir = false
                await fs.promises.mkdir(dir)
            }

            const document = createfinalDocument(baseText, extraData, tableInfo[companyName], userData);

            Packer.toBuffer(document).then((buffer) => {
                if (!validatorDir) {
                    fs.writeFileSync(`${dir}/${companyName}.docx`, buffer);
                } else {
                    fs.writeFileSync(`${dir}/${companyName}.docx`, buffer);

                }
            });
            console.log(`[${new Date}] Document Creator : Document/s created!   // Number of documents: ${1}`.green);
            res.status(200).json({ msj: `Document created!`, status: 200 })

        } else {
            res.status(403).json({ msj: `User's data not found.Sorry...`, status: 404 })
            console.log(`[${new Date}] User Validator  : Error! // Error: ${response.text}`.red);
        }
    } else {
        res.status(403).json({ msj: 'Token Invalid!!', status: 403 })
        console.log(`[${new Date}] User Validator  : Error! // Error: Token Invalid!!`.red);
    }
}

module.exports = {
    createDocumentsMiddle
}