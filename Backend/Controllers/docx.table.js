const { TableRow, Table, TableCell, Paragraph, WidthType, TextRun, AlignmentType } = require('docx');

const addDataTable = (data) => {
    return new TableRow({
        children: [
            new TableCell({
                width: {
                    size: 1000,
                    type: WidthType.DXA
                },
                children: [new Paragraph({
                    children: [
                        new TextRun({
                            text: data.product,
                            bold: true,
                            size: 12 * 2,
                            font: 'Times New Roman',
                        })
                    ],
                    alignment: AlignmentType.CENTER,

                }
                )],
                borders: {
                    right: {
                        space: 4000
                    }
                }
            }),
            new TableCell({
                width: {
                    size: 1000,
                    type: WidthType.DXA
                },
                children: [
                    new Paragraph({
                        children: [
                            new TextRun({
                                text: data.quantity.quant,
                                bold: true,
                                size: 12 * 2,
                                font: 'Times New Roman',

                            }),
                            new TextRun({
                                text: (data.quantity.unit).toUpperCase() + '/S',
                                bold: true,
                                size: 12 * 2,
                                font: 'Times New Roman',
                                break: 1
                            }),
                        ],
                        alignment: AlignmentType.CENTER
                    })
                ],

            }),
            new TableCell({
                width: {
                    size: 1700,
                    type: WidthType.DXA
                },
                children: [new Paragraph({
                    children: [
                        new TextRun({
                            text: data.CU.yen,
                            bold: true,
                            size: 12 * 2,
                            font: 'Times New Roman',

                        }),
                        new TextRun({
                            text: data.CU.us,
                            bold: true,
                            size: 12 * 2,
                            font: 'Times New Roman',
                            break: 1
                        }),
                    ],
                    alignment: AlignmentType.CENTER
                })
                ]
            }),
            new TableCell({
                width: {
                    size: 1600,
                    type: WidthType.DXA
                },
                children: [new Paragraph({
                    children: [
                        new TextRun({
                            text: data.CT.yen,
                            bold: true,
                            size: 12 * 2,
                            font: 'Times New Roman',

                        }),
                        new TextRun({
                            text: data.CT.us,
                            bold: true,
                            size: 12 * 2,
                            font: 'Times New Roman',
                            break: 1
                        }),
                    ],
                    alignment: AlignmentType.CENTER
                })
                ]
            }),
            new TableCell({
                width: {
                    size: 1000,
                    type: WidthType.DXA
                },
                children: [new Paragraph({
                    children: [
                        new TextRun({
                            text: data.cert,
                            bold: true,
                            size: 12 * 2,
                            font: 'Times New Roman'
                        })
                    ],
                    alignment: AlignmentType.CENTER
                }
                )]
            }),
            new TableCell({
                width: {
                    size: 2000,
                    type: WidthType.DXA
                },
                children: [new Paragraph({
                    children: [
                        new TextRun({
                            text: data.annot,
                            bold: true,
                            size: 12 * 2,
                            font: 'Times New Roman'
                        })
                    ],
                    alignment: AlignmentType.CENTER
                }
                )]
            }),

        ]
    });

}

const createFinalTable = (tableInfo) => {
    const baseRow = new TableRow({
        children: [
            new TableCell({
                width: {
                    size: 1500,
                    type: WidthType.DXA
                },
                children: [new Paragraph({
                    children: [
                        new TextRun({
                            text: 'Product',
                            bold: true,
                            size: 14 * 2,
                            font: 'Times New Roman'
                        })
                    ],
                    alignment: AlignmentType.CENTER
                }
                )]
            }),
            new TableCell({
                width: {
                    size: 1200,
                    type: WidthType.DXA
                },
                children: [new Paragraph({
                    children: [
                        new TextRun({
                            text: 'Quantity',
                            bold: true,
                            size: 14 * 2,
                            font: 'Times New Roman'
                        })
                    ],
                    alignment: AlignmentType.CENTER
                }
                )]
            }),
            new TableCell({
                width: {
                    size: 1700,
                    type: WidthType.DXA
                },
                children: [new Paragraph({
                    children: [
                        new TextRun({
                            text: 'Cost per Unit',
                            bold: true,
                            size: 14 * 2,
                            font: 'Times New Roman'
                        })
                    ],
                    alignment: AlignmentType.CENTER
                }
                )]
            }),
            new TableCell({
                width: {
                    size: 1600,
                    type: WidthType.DXA
                },
                children: [new Paragraph({
                    children: [
                        new TextRun({
                            text: 'Cost ',
                            bold: true,
                            size: 14 * 2,
                            font: 'Times New Roman',

                        }),
                        new TextRun({
                            text: 'Total',
                            bold: true,
                            size: 14 * 2,
                            font: 'Times New Roman',
                            break: 1
                        }),
                    ],
                    alignment: AlignmentType.CENTER
                }
                )]
            }),
            new TableCell({
                width: {
                    size: 1600,
                    type: WidthType.DXA
                },
                children: [new Paragraph({
                    children: [
                        new TextRun({
                            text: 'Certificated',
                            bold: true,
                            size: 14 * 2,
                            font: 'Times New Roman'
                        })
                    ],
                    alignment: AlignmentType.CENTER
                }
                )]
            }),
            new TableCell({
                width: {
                    size: 3000,
                    type: WidthType.DXA
                },
                children: [new Paragraph({
                    children: [
                        new TextRun({
                            text: 'Annotations',
                            bold: true,
                            size: 14 * 2,
                            font: 'Times New Roman'
                        })
                    ],
                    alignment: AlignmentType.CENTER
                }
                )]
            }),

        ]
    });

    const table = new Table({
        columnWidths: [1000, 5000],
        rows: [
            baseRow
        ],
    });

    tableInfo.map(data => {
        table.addChildElement(addDataTable(data))
    })

    return table
}

module.exports = {
    createFinalTable
};