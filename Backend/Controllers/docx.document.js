const { Document } = require('docx');
const { createFooter } = require('./docx.footer');
const { createParraph } = require('./docx.paragraph');
const { createFinalTable } = require('./docx.table');

const createDirectoryName = () => {
    let day = new Date();
    let month = new Date();
    let year = new Date();

    day = day.getDate();
    month = month.getMonth() + 1;
    year = year.getFullYear();

    
    const fullDate = `${year}-${month}-${day}`;

    return fullDate;
}


createDirectoryName()
const createfinalDocument = (baseText, extraDataBaseText, tableInfo, userData) => {
    return new Document({
        sections: [{
            children: [createParraph(baseText, extraDataBaseText), createFinalTable(tableInfo)],
            footers: {
                default: createFooter(userData)
            }

        }]
    })
}



module.exports = {
    createfinalDocument,
    createDirectoryName
}