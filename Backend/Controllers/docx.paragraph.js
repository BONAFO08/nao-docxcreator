const { Paragraph, TextRun, AlignmentType } = require('docx');


const cleanBaseText = (baseText,extraData) => {

    let arrFood = baseText;
    let indexSTART;
    let indexEND;
    let objectAux;
    let newArrFood = [];

    for (let i = 0; i < 70; i++) {
        arrFood.trim()
        indexSTART = arrFood.indexOf('//');
        objectAux = arrFood.substring(0, indexSTART);
        arrFood = arrFood.replace(objectAux, '');
        arrFood = arrFood.replace('//', '');
        if (objectAux != '') {
            switch (objectAux) {
                case 'companyName':
                    newArrFood.push({ text: extraData.companyName, italics: true, bold: true });
                    break
                case "date":
                    newArrFood.push({ text: extraData.dateSelected, italics: false, bold: true });
                    break
                default:
                    newArrFood.push({ text: objectAux, italics: false, bold: false });
                    break
            }
        }
    }
    return newArrFood
}


const createParraph = (baseText,extraData) => {
    const paraph = new Paragraph({
        children: cleanBaseText(baseText,extraData).map(text => {
            return new TextRun({
                text: text.text,
                size: 14 * 2,
                font: 'Times New Roman',
                italics: text.italics,
                bold: text.bold,
            })
        }),
        alignment: AlignmentType.LEFT
    })

    paraph.addChildElement(new TextRun({
        text: '',
        bold: true,
        size: 14 * 2,
        font: 'Times New Roman',
        break: 3
    }))
    return paraph

}

module.exports = {
    createParraph
}