require('dotenv').config();
const { findByEmail, findByUsername, findById } = require("./searchData");
const { User } = require("../Config/database.config");
const bcryptjs = require('bcryptjs');
const jwt = require('jsonwebtoken');

//Hash password
const cripty = async (userData) => {
    let pass = await bcryptjs.hash(userData, 8);
    return pass;
}

//Compare password
const decripty = async (userData, old) => {
    let aux = await bcryptjs.compare(userData, old);
    return aux;
}

//Create token
const newTokensing = async (userData) => {
    userData._id = userData._id.toString();
    const token = jwt.sign(userData, process.env.JWT_SECRET, { expiresIn: '24h' });
    return token;

}

//Decrypt token
const consumeToken = (token) => {
    try {
        let cleantoken = token;
        cleantoken = cleantoken.replace('Bearer ', '');
        const detoken = jwt.verify(cleantoken, process.env.JWT_SECRET);
        return detoken;
    } catch (error) {
        return false;
    }
}

const createUser = async (userData) => {
    const msj = {};

    const validatorEmail = await findByEmail(User, userData.email);
    const validatorUsername = await findByUsername(User, userData.username);

    (validatorUsername != false) ? (msj.text = `The username is already taken.`, msj.status = 403) : ('');
    (validatorEmail != false) ? (msj.text = `The email is already taken.`, msj.status = 403) : ('');
    (validatorEmail != false && validatorUsername != false) ? (msj.text = `The username and email already exist.`, msj.status = 403) : ('');

    if (validatorUsername == false && validatorEmail == false) {
        const passwordEncrypted = await cripty(userData.password);


        const newUser = await new User({
            username: userData.username,
            name: userData.name,
            surname: userData.surname,
            email: userData.email,
            companyId: userData.companyId,
            dni: userData.dni,
            password: passwordEncrypted,
        }
        );

        const result = await newUser.save();

        return { text: result, status: 200 };
    } else {
        return msj
    }
}

const validateUser = async (userData) => {
    const msj = {
        text: 'Invalid Credentials',
        boolean: false,
        status: 403,
    };

    (userData.username.includes('@') && userData.username.includes('.com'))
        ? (msj.boolean = await findByEmail(User, userData.username))
        : (msj.boolean = await findByUsername(User, userData.username));


    const dataUser = msj.boolean[0];

    if (msj.boolean != false) {
        msj.boolean = await decripty(userData.password, dataUser.password);
        if (msj.boolean != false) {
            let dataToken = {
                _id: dataUser._id,
            }
            await newTokensing(dataToken)
                .then(resolve => msj.token = resolve);
            msj.boolean = true;
            msj.status = 200;
        }
    }
    return msj;

}

//Validate a token
const validateToken = (token) => {
    let desToken = consumeToken(token);

    if (desToken != false) {
        return desToken;
    } else {
        return false;
    }
}


//Show the data of a user
const searchDataUser = async (id) => {
    const msj = {
        status: 0,
        txt: '',
    };


    const dataUser = await findById(User, id);

    if (dataUser != false) {
        msj.txt = dataUser[0];
        msj.status = 200;
    } else {
        msj.txt = 'User not found';
        msj.status = 404;
    }
    return msj;
}


const validateUpdateData = (newData) => {
    let validator = false;
    const admitedData = ['name', 'surname', 'email', 'companyId', 'dni'];
    const userDataKey = Object.keys(newData);
    userDataKey.map(key => {
        const isTheDataAdmited = admitedData.filter(keyAdmited => keyAdmited == key)
        if (newData[key] != undefined && isTheDataAdmited.length != 0) {
            validator = validator || true
        } else {
            validator = validator || false
        }
    })
    return validator
}



//Update the data of a user
const updateDataUser = async (newData, id) => {
    const msj = {
        status: 400,
        txt: 'Error.Please contact with the server.',
    };

    const validator = validateUpdateData(newData)

    const originalData = await findById(User, id);

    if (originalData != false && validator != false) {

        let validateEmail = true;
        if (newData.email != undefined) {
            validateEmail =
                (await findByEmail(User, newData.email) != false)
                    ? (false)
                    : (true)
        }

        if (validateEmail != false) {
            const userUpdated = await User.updateMany({ _id: id },
                {
                    $set: {
                        name: (newData.name == undefined) ? (originalData.name) : (newData.name),
                        surname: (newData.surname == undefined) ? (originalData.surname) : (newData.surname),
                        email: (newData.email == undefined) ? (originalData.email) : (newData.email),
                        companyId: (newData.companyId == undefined) ? (originalData.companyId) : (newData.companyId),
                        dni: (newData.dni == undefined) ? (originalData.dni) : (newData.dni),

                    }
                }
            );
            msj.txt = 'Data updated!';
            msj.status = 200;
        } else {
            msj.txt = 'The email is already taken';
            msj.status = 409;
        }
    } else {
        msj.txt = 'User not found or nothing to update';
        msj.status = 404;
    }
    return msj;
}


module.exports = {
    createUser,
    validateUser,
    validateToken,
    searchDataUser,
    updateDataUser

}