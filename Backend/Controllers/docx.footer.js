const { Paragraph, TextRun, AlignmentType, Footer, ImageRun, HorizontalPositionRelativeFrom, VerticalPositionRelativeFrom } = require('docx');
const fs = require("fs");


const createFooter = (userData) => {
    return new Footer({
        children: [
            new Paragraph({
                children: [
                    new TextRun({
                        text: userData.name,
                        size: 12 * 2,
                    }),
                    new TextRun({
                        text: userData.surname,
                        size: 12 * 2,
                        break: 1
                    }),
                    new TextRun({
                        text: userData.companyId,
                        size: 12 * 2,
                        break: 1
                    }),
                    new TextRun({
                        text: userData.dni,
                        size: 12 * 2,
                        break: 1
                    }),
                ],
                alignment: AlignmentType.LEFT
            }),
            new Paragraph({
                children: [
                    new ImageRun({
                        data: fs.readFileSync("./Images/logo.png"),
                        transformation: {
                            width: 100,
                            height: 100,
                        },
                        floating: {
                            horizontalPosition: {
                                relative: HorizontalPositionRelativeFrom.RIGHT_MARGIN,
                                offset: -400000,
                            },
                            verticalPosition: {
                                relative: VerticalPositionRelativeFrom.BOTTOM_MARGIN,
                                offset: 0,
                            }

                        }

                    })
                ],
                alignment: AlignmentType.RIGHT
            })
        ]
    })

}

module.exports = {
    createFooter
}