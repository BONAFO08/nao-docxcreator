const express = require('express');
const { createDocumentsMiddle } = require('../Middles/docx.middle');
const router = express.Router();


/**
 * @swagger
 * /docx/createDocuments:
 *  post:
 *    tags: 
 *      [Docx]
 *    summary: Create Documents
 *    description: Create X number of Documents (it's determinated by the number of companiesNames)
 *    parameters:
 *    - name: authorization
 *      description: User access token
 *      in: header
 *      required: false
 *      type: string
 *    responses:
 *            200:
 *                description: Documents Created 
 *            403:
 *                description: Wrong Credentials
 * 
 */


 router.post("/docx/createDocuments", (req, res) => {
    createDocumentsMiddle(req,res)
});



module.exports = router;
