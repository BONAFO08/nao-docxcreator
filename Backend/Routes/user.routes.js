const express = require('express');
const { signUp, logIn, validateTokenMiddle, getUserDataMiddle, saveNewUserDataMiddle } = require('../Middles/user.middle');
const router = express.Router();


/**
 * @swagger
 * /user/signUp:
 *  post:
 *    tags: 
 *      [User]
 *    summary: Sign Up
 *    description: Create an user account
 *    parameters:
 *    - name: username
 *      description: Username
 *      in: formData
 *      required: false
 *      type: string
 *    - name: name
 *      description: Only names
 *      in: formData
 *      required: false
 *      type: string
 *    - name: surname
 *      description: Only surnames
 *      in: formData
 *      required: false
 *      type: string
 *    - name: email
 *      description: User email
 *      in: formData
 *      required: false
 *      type: string
 *    - name: companyId
 *      description: Company personal ID
 *      in: formData
 *      required: false
 *      type: string
 *    - name: dni
 *      description: Personal ID
 *      in: formData
 *      required: false
 *      type: number
 *    - name: password
 *      description: Password
 *      in: formData
 *      required: false
 *      type: string
 *    responses:
 *            200:
 *                description: User created
 *            409:
 *                description: User already exist
 *            403:
 *                description: Data invalid.
 * 
 */


router.post("/user/signUp", (req, res) => {
    signUp(req, res)
});

/**
 * @swagger
 * /user/logIn:
 *  post:
 *    tags: 
 *      [User]
 *    summary: Log In
 *    description: Login an user and send the JWT
 *    parameters:
 *    - name: username
 *      description: Username
 *      in: formData
 *      required: false
 *      type: string
 *    - name: password
 *      description: Password
 *      in: formData
 *      required: false
 *      type: string
 *    responses:
 *            200:  
 *                description: User Identificated
 *            403:
 *                description: Invalid Credentials.
 * 
 */


router.post("/user/logIn", (req, res) => {
    logIn(req, res)
});



/**
 * @swagger
 * /user/validateToken:
 *  get:
 *    tags: 
 *      [User]
 *    summary: Valitade token
 *    description: Only validate the JWT and send status
 *    parameters:
 *    - name: authorization
 *      description: User access token
 *      in: header
 *      required: false
 *      type: string
 *    responses:
 *            200:  
 *                description: User Identificated
 *            403:
 *                description: Invalid Token.
 * 
 */


router.get("/user/validateToken", (req, res) => {
    validateTokenMiddle(req, res)
});


/**
 * @swagger
 * /user/userData:
 *  get:
 *    tags: 
 *      [User]
 *    summary: Send user's data
 *    description: Send the data associated with the ID inside of the Token
 *    parameters:
 *    - name: authorization
 *      description: User access token
 *      in: header
 *      required: false
 *      type: string
 *    responses:
 *            200:  
 *                description: User Identificated
 *            403:
 *                description: Invalid Token.
 * 
 */


router.get("/user/userData", (req, res) => {
    getUserDataMiddle(req, res)
});


/**
 * @swagger
 * /user/updateUser:
 *  put:
 *    tags: 
 *      [User]
 *    summary: Update user's data
 *    description: Modify the data associated with the ID inside of the Token
 *    parameters:
 *    - name: authorization
 *      description: User access token
 *      in: header
 *      required: false
 *      type: string
 *    - name: name
 *      description: Only names
 *      in: formData
 *      required: false
 *      type: string
 *    - name: surname
 *      description: Only surnames
 *      in: formData
 *      required: false
 *      type: string
 *    - name: email
 *      description: User email
 *      in: formData
 *      required: false
 *      type: string
 *    - name: companyId
 *      description: Company personal ID
 *      in: formData
 *      required: false
 *      type: string
 *    - name: dni
 *      description: Personal ID
 *      in: formData
 *      required: false
 *      type: number
 *    responses:
 *            200:  
 *                description: User Identificated
 *            403:
 *                description: Invalid Token.
 * 
 */


router.put("/user/updateUser", (req, res) => {
    saveNewUserDataMiddle(req, res)
});



module.exports = router;
