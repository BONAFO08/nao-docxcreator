require('dotenv').config();
const mongoose = require('mongoose');
const userSchema = require('../Models/user.model');

const options = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
}

const uri = process.env.MONGO_ATLAS_URL;

mongoose.connect(uri);

const User = mongoose.model('users', userSchema);

module.exports = {
    mongoose,
    User,
};